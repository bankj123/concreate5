<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<head>
  <?php Loader::element('header_required') ?>
  <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0, minimal-ui">
  <meta name="apple-mobile-web-app-capable" content="yes" />

	<title>B Power</title>
	<meta name="description" content="หนึ่งในผู้นำด้านการผลิต และจัดจำหน่ายผลิตภัณฑ์ตะแกรงเหล็กสำเร็จรูป ลวดเหล็กรีดเย็น ตะแกรงลวดเหล็กกล้าเสริมคอนกรีต
ประสบการณ์ในการผลิตตะแกรงเหล็กสำเร็จรูปมานานกว่า 15 ปี ทั้งการผลิตโดยเครื่องจักรที่ทันสมัย
ผสานเทคโนโลยีพร้อมระบบตรวจสอบ และควบคุมโดยวิศวกรผู้เชี่ยวชาญ">
  <meta name="keywords" content="B power,บี พาวเวอร์">

  <!-- GOOGLE PLUS -->
  <meta itemprop="name" content="B Power">
  <meta itemprop="description" content="หนึ่งในผู้นำด้านการผลิต และจัดจำหน่ายผลิตภัณฑ์ตะแกรงเหล็กสำเร็จรูป ลวดเหล็กรีดเย็น ตะแกรงลวดเหล็กกล้าเสริมคอนกรีต
ประสบการณ์ในการผลิตตะแกรงเหล็กสำเร็จรูปมานานกว่า 15 ปี ทั้งการผลิตโดยเครื่องจักรที่ทันสมัย
ผสานเทคโนโลยีพร้อมระบบตรวจสอบ และควบคุมโดยวิศวกรผู้เชี่ยวชาญ">
  <meta itemprop="image" content="<?= $view->getThemePath() ?>/assets/img/share-img/og-1200x630.jpg">
  <meta itemprop="url" content="http://ec2-3-0-177-180.ap-southeast-1.compute.amazonaws.com/">
  <meta itemprop="alternateName" content="B Power">
  
  <!-- Meta Open Graph (FB) -->
  <meta property="og:type" content="website">
  <meta property="og:url" content="http://ec2-3-0-177-180.ap-southeast-1.compute.amazonaws.com/">
  <meta property="og:title" content="B Power">
  <meta property="og:description" content="หนึ่งในผู้นำด้านการผลิต และจัดจำหน่ายผลิตภัณฑ์ตะแกรงเหล็กสำเร็จรูป ลวดเหล็กรีดเย็น ตะแกรงลวดเหล็กกล้าเสริมคอนกรีต
ประสบการณ์ในการผลิตตะแกรงเหล็กสำเร็จรูปมานานกว่า 15 ปี ทั้งการผลิตโดยเครื่องจักรที่ทันสมัย
ผสานเทคโนโลยีพร้อมระบบตรวจสอบ และควบคุมโดยวิศวกรผู้เชี่ยวชาญ">
  <meta property="og:image" content="<?= $view->getThemePath() ?>/assets/img/share-img/og-1200x630.jpg">
  <meta property="og:site_name" content="B Power">
  
  <!-- Twitter Card -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="B Power">
  <meta name="twitter:description" content="หนึ่งในผู้นำด้านการผลิต และจัดจำหน่ายผลิตภัณฑ์ตะแกรงเหล็กสำเร็จรูป ลวดเหล็กรีดเย็น ตะแกรงลวดเหล็กกล้าเสริมคอนกรีต
ประสบการณ์ในการผลิตตะแกรงเหล็กสำเร็จรูปมานานกว่า 15 ปี ทั้งการผลิตโดยเครื่องจักรที่ทันสมัย
ผสานเทคโนโลยีพร้อมระบบตรวจสอบ และควบคุมโดยวิศวกรผู้เชี่ยวชาญ">
  <meta name="twitter:url" content="http://ec2-3-0-177-180.ap-southeast-1.compute.amazonaws.com/">
  <meta name="twitter:image:src" content="<?= $view->getThemePath() ?>/assets/img/share-img/og-630x630.jpg">

	<link rel="icon" type="image/png" href="<?= $view->getThemePath() ?>/assets/img/share/favicon.png">

  <link rel="stylesheet" href="<?= $view->getThemePath() ?>/assets/css/normalize.min.css">
	<link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">
	<link rel="stylesheet" href="<?= $view->getThemePath() ?>/assets/css/main.css">
	<link rel="stylesheet" href="<?= $view->getThemePath() ?>/assets/css/responsive.css">

	<!--[if lte IE 8]>
  	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <style>
        header{
          z-index: 999 !important;
        }

    </style>
</head>