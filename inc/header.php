<header id="header">
	<div class="page-center">
		<a href="#" class="hamburger">
			<div></div>
		</a>

		<h1 class="logo"><a href="./"></a></h1>

		<div class="box-search  outer">
			<a href="#" class="btn-search"></a>
			<div class="form-style">
				<form action="">
					<input type="text" class="inputtext-style" placeholder="Search">
				</form>
			</div>
		</div>

		<div class="group-menu-header">
			<nav class="mainmenu">
				<div class="col">
					<a href="<?php echo View::url('/'); ?>">Home</a>
					<a href="<?php echo View::url('/about'); ?>">About us</a>
				</div>
				<div class="col">
					<a href="<?php echo View::url('/product-service'); ?>">Product & Service</a>
					<a href="<?php echo View::url('/project-reference'); ?>">Project Reference</a>
				</div>
				<div class="col">
					<a href="<?php echo View::url('/#contact-us'); ?>">Contact us</a>
				</div>
				<!-- <div class="col">
					<div class="menu-news">
						<a href="/news">News</a>
						<div class="submenu">
							<a href="#">Press Release and Event</a>
							<a href="#">Construction Update</a>
						</div>
					</div>
					<a href="contact-us">Contact</a>
				</div> -->
			</nav>

			<div class="tools">
				<!-- <div class="box-search">
					<a href="#" class="btn-search"></a>
					<div class="form-style">
						<form action="">
							<input type="text" class="inputtext-style" placeholder="Search">
						</form>
					</div>
				</div> -->

				<!-- <div class="language">
					<p>Language :</p>
					<a href="#" class="active">EN</a>
					<a href="#">TH</a>
				</div> -->

				<!-- <a href="/register" class="btn-register">REGISTER</a> -->
			</div>
		</div>
	</div>

	<nav class="menu-page">
		<ul>
			<li>
				<a href="#about-the-parq">เกี่ยวกับ B POWER</a>
			</li>
			<li>
				<a href="#gallery">แกลเลอรี่</a>
			</li>
			<li>
				<a href="#design-concept">กลยุทธ์</a>
			</li>
			<li>
				<a href="#building-specification-master-plan">ข้อมูลทั่วไป</a>
			</li>
		</ul>
	</nav>

	<nav class="menu-page  -home">
		<ul>
			<li>
				<a href="#welcome">Welcome</a>
			</li>
			<li>
				<a href="#about-bpower">เกี่ยวกับ บี พาวเวอร์</a>
			</li>
			<li>
				<a href="#product-service">สินค้าและบริการ</a>
			</li>
			<li>
				<a href="#product-bpower">สินค้าของ บี พาวเวอร์</a>
			</li>
			<li>
				<a href="#contact-us">ติดต่อเรา</a>
			</li>
		</ul>
	</nav>
</header>