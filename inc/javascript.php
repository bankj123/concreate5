<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php if ($c->isEditMode()) { ?>

<?php } else { ?>
  <!-- main -->
  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/jquery-3.1.1.min.js"></script>

  <!-- plugin -->
  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/jquery.mousewheel.min.js"></script>
  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/jquery.easing.1.3.min.js"></script>
  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/TweenMax.min.js"></script>

  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/jquery.browser.min.js"></script>

  <link rel="stylesheet" href="<?= $view->getThemePath() ?>/assets/js/jquery.fancybox/jquery.fancybox.min.css">
  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/jquery.fancybox/jquery.fancybox.min.js"></script>

  <link rel="stylesheet" href="<?= $view->getThemePath() ?>/assets/js/owl.carousel/owl.carousel.min.css">
  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/owl.carousel/owl.carousel.min.js"></script>

  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/jquery.bxslider.min.js"></script>

  <!-- <script type="text/javascript" src="assets/js/jquery.parallax-scroll.min.js"></script> -->

  <!-- <script type="text/javascript" src="assets/js/jquery.visible.min.js"></script> -->

  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/jquery.scrollify.min.js"></script>

  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/jquery.nav.min.js"></script>

  <script type="text/javascript" src="<?= $view->getThemePath() ?>/assets/js/main.js"></script>
<?php } ?>