<?php
defined('C5_EXECUTE') or die("Access Denied.");
define('URL_REWRITING_ALL', true); ?>
<!DOCTYPE html>
<html>

<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('inc/head.php'); ?>

<body>
	<div class="<?= $c->getPageWrapperClass() ?>">
		<!--#wrapper-->
		<div id="wrapper" class="about-page">
			<!-- header => [menu, share top content] -->
			<?php
			defined('C5_EXECUTE') or die("Access Denied.");
			$this->inc('inc/header.php'); ?>
			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
			<div id="about-the-parq" class="section  highlight-content  bg-rectangle-white">
				<div class="page-center">
					<div class="title-section">
						<div class="underline">
							<h1>เกี่ยวกับ <span>B POWER</span></h1>
						</div>
					</div>
					<img src="<?= $view->getThemePath() ?>/assets/img/about/img-synergy-of-life.png" alt="" class="bg">
				</div>
			</div>

				<div id="project-vision" class="section  description-highlight-content  project-vision  bg-wood-cream">
				<div class="page-center">
					<!-- <div class="content"> -->
						<!-- <p>Steeped in a history of commerce, industry, trade, culture and community, the <span class="green">QSNCC node</span> is set to embrace a second major catalyst for its regeneration and ride a new wave of <span class="green">urban development</span>. Built over two phases, commercial offices at <span class="green">THE PARQ</span> offer Bangkok’s largest column-free floorplates, serving as a platform for domestic and multinational companies to anchor and connect their businesses, engage their employees and create new value within the city’s premier commercial complex.</p> -->
						<!-- </div> -->
					<div class="description">
						<p>บริษัท บี เพาเวอร์ แฟคตอรี่ แอนด์ คอนสตรัคชั่น จำกัด มีความมุ่งมั่นที่จะผลิตสินค้าที่มีคุณภาพสูง พร้อมทั้งการพัฒนาสินค้าใหม่ๆอย่างต่อเนื่อง เพื่อสนองความต้องการแก่ลูกค้า ให้ได้อย่างมีประสิทธิภาพ และคงไว้ซึ่งความพึงพอใจของลูกค้า</p>
						<p>การผลิต ผลิตภัณฑ์ทุกชนิด ในทุกขั้นตอน จะคำนึงถึงคุณภาพเป็นหลัก โดยยึดถือตามมาตรฐานต่างๆ ที่อ้างอิง และแผนคุณภาพ ตามระบบบริหารคุณภาพ</p>
						<p>บริษัท บี เพาเวอร์ แฟคตอรี่ แอนด์ คอนสตรัคชั่น จำกัด มีกำลังการผลิตมากกว่า 40,000 ตันต่อปี โรงงานตั้งอยู่ เขตอุตสาหกรรมกบินทร์บุรี จังหวัดปราจีนบุรี</p>
						<p>บริษัทมีเป้าหมายการดำเนินธุรกิจ คือ “เป็นผู้นำด้านการผลิต และจัดจำหน่าย รวมถึงการบริการด้านการติดตั้งอย่างครบวงจรของรั้วตะแกรงเหล็กสำเร็จรูป (ezy-fence) และเหล็กสำเร็จรูปสำหรับงานก่อสร้าง (Cut&Bend, Wire Mesh, Engineering Mesh) และสินค้าอุตสาหกรรมเหล็กก่อสร้างอื่นๆ เพื่อรองรับการเติบโตของการก่อสร้างหลักในประเทศไทย และภูมิภาคอาเซียน ” ภายใต้เป้าหมายดังกล่าวบริษัทได้ดำเนินกลยุทธ์เพื่อการสร้างมูลค่าเพิ่ม (Value Added) ให้กับผลิตภัณฑ์เหล็กสำเร็จรูปเพื่อการก่อสร้าง เพิ่มความสะดวก รวดเร็ว ให้กับลูกค้า ดังต่อไปนี้</p>
						<div class="key-wellness-features">
							<div class="item">
								<ul class="list-bullet">
									<li>รักษาคุณภาพของสินค้าและบริการของบริษัท (Top quality Product and Service)</li>
									<li>พัฒนาความเชี่ยวชาญในตัวผลิตภัณฑ์อย่างต่อเนื่อง (Product Specialist/ Product Professional)</li>
									<li>เพิ่มผลิตภัณฑ์และบริการให้ครบวงจร (One Stop Services Solutions)</li>
									<li>ลูกค้าและผู้รับเหมาก่อสร้าง ถือเป็นหุ้นส่วนทางการค้าของบริษัท (Partnership) </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

				<div id="gallery" class="section  gallery  bg-wood-green">
					<div class="title-section  light-style">
						<div class="underline">
							<h1>แกลเลอรี่</h1>
						</div>
					</div>
					<div class="slide-gallery">
						<div class="owl-carousel">
							<div class="item">
								<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
							</div>
							<div class="item">
								<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
							</div>
							<div class="item">
								<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
							</div>
							<div class="item">
								<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
							</div>
						</div>
					</div>
				</div>

				<div id="design-concept" class="section  design-concept  bg-wood-white">
				<div class="page-center">
					<div class="title-section">
						<div class="underline">
							<h1><span>กลยุทธ์</span></h1>
						</div>
					</div>
					<div class="content">
						<div class="description">
						<p>กลยุทธ์การขยายตัวในแนวตั้งโดยการทำธุรกิจที่เกี่ยวข้องกับธุรกิจเดิม เพื่อแย่งชิงส่วนแบ่งการตลาดให้มากขึ้น และเพิ่มสายการผลิตส่วน Cut&Bend เพื่อขยายกลุ่มลูกค้าที่กำลังจะเติบโตในอุตสาหกรรมก่อสร้างโดย</p>
						<div class="key-wellness-features">
							<div class="item">
								<ul class="list-bullet">
									<li>รักษาคุณภาพของสินค้าและบริการของบริษัท (Top quality Product and Service)</li>
									<li>พัฒนาความเชี่ยวชาญในตัวผลิตภัณฑ์อย่างต่อเนื่อง (Product Specialist/ Product Professional)</li>
									<li>เพิ่มผลิตภัณฑ์และบริการให้ครบวงจร (One Stop Services Solutions)</li>
									<li>ลูกค้าและผู้รับเหมาก่อสร้าง ถือเป็นหุ้นส่วนทางการค้าของบริษัท (Partnership) </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>

				<div id="building-specification-master-plan" class="section  master-plan  bg-rectangle-white">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1>ข้อมูล <span>ทั่วไป</span></h1>
							</div>
						</div>
						<div class="content">
							<div class="description">
								<div class="col">
									<p><strong>ชื่อบริษัท :</strong> บริษัท บี เพาเวอร์ แฟคตอรี่แอนด์ คอนสตรัคชั่น จำกัด</p>
									<p><strong>ชื่อย่อ :</strong> BPW</p>
									<p><strong>ปีที่ก่อนตั้ง :</strong> พ.ศ. 2551</p>
									<p><strong>ประเภทธุรกิจ :</strong> ผลิตและจำหน่ายรั้วเหล็กสำเร็จรูป เหล็กสำเร็จรูปสำหรับงานก่อสร้าง</p>
									<p><strong>ที่ตั้งสำนักงาน :</strong> 2/9 อาคารบางนาคอมเพล็กซ์ ชั้น 4 ซอยบางนา-ตราด 25 แขวงบางนา เขตบางนา กรุงเทพมหานคร 10260</p>
								</div>
								<div class="col">
									<p><strong>ที่ตั้งโรงงาน :</strong> 450/3 หมู่ 9 นิคมอุตสาหกรรมกบินทร์บุรี ต.หนองกี่ อ.กบินทร์บุรี จ.ปราจีนบุรี 25110</p>
									<p><strong>เลขทะเบียนบริษัท :</strong> 0105551104205</p>
									<p><strong>โทรศัพท​์ (สำนักงาน):</strong> 02 361 8470</p>
									<p><strong>โทรสาร (สำนักงาน):</strong> 02 361 8469</p>
									<p><strong>Website :</strong> bpower.co.th</p>
									<p><strong>ทุนจนทะเบียน :</strong> 40,000,000 บาท</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php
			defined('C5_EXECUTE') or die("Access Denied.");
			$this->inc('inc/footer.php'); ?>

		</div>
		<!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php
		defined('C5_EXECUTE') or die("Access Denied.");

		$this->inc('inc/javascript.php'); ?>

		<!-- start javascript this page -->
		<script>
			runScriptAboutPage();
		</script>
	</div>
	<!-- end javascript this page -->
	<?php Loader::element('footer_required') ?>
</body>

</html>