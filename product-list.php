<!DOCTYPE html>
<html>
<?php include('inc/head.php'); ?>

<body>
	<!--#wrapper-->
	<div id="wrapper" class="well-building-page">
		<!-- header => [menu, share top content] -->
		<?php include('inc/header.php'); ?>

		<!-- start content this page -->
		<!--#container-->
		<div role="main" id="container">
			<div id="product-service" class="bg-rectangle-white">
				<div class="page-center">
					<div id="product-bpower" class="box-core-features" style="margin: 80px auto 80px;">
						<div class="title-section">
							<div class="underline">
							<h1>สินค้าของ <span>บี พาวเวอร์</span></h1>
						</div>
						</div>
						<div class="list-core-features">
						<a href="#" class="item">
								<span class="img  hover-img" style="max-height:100px">
									<img src="http://www.billionmesh.com/UserFiles/image/ezyfence08.jpg" alt="">
								</span>
								<span class="detail">
									<span class="icon">
										<img src="<?= $view->getThemePath() ?>/assets/img/icons/comfort.png" alt="">
									</span>
									<span class="text">
										<p>Fence</p>
									</span>
								</span>
								<div class="item-content">
									<span class="row">
										<h2>Ezy-Fence</h2>
										<ul class="list-bullet  -bold">
											<li>
												ผลิตจากลวดรีดเย็นคุณภาพสูงมาตรฐาน มอก.747-2531 อาร์คไฟฟ้า ตัดและดัดด้วยเครื่องจักรอัตโนมัติที่มีความละเอียดสูง ควบคุมการผลิตด้วยระบบคอมพิวเตอร์ มอก.737-2549 มีระบบการเคลือบผิวให้เลือก 2 ระบบ
												<ul>
													<li>วิธีการเคลือบผิวด้วยการชุบกัลวาไนซ์ (Hot-Dip Galvanized) ที่อุณหภูมิไม่ต่ำกว่า 450 องศาเซลเซียส เพื่อให้ผิวเหล็กและสังกะสีละลายติดกันเป็นเนื้อเดียว มีประสิทธิภาพการป้องกันสนิมสูงสุด ทนต่อการการกัดกร่อนของน้ำเกลือ โดยมีมาตรฐานสากลรับรองการชุบกัลวาไนซ์ ASTM A123</li>
													<li>วิธีการเคลือบผิววัสดุด้วยวิธี Powder Coating ที่ผ่านกระบวนการชุบ Electro-Deposition Paint (EDP) ที่มีความหนาไม่ต่ำกว่า 100 ไมครอน ทนต่อรังสีอุลตร้าไวโอเล็ต สีสวยสด แข็งแรงทนทาน และไม่เป็นสนิม มีให้เลือกทั้งหมด 5 สี คือ สีขาว สีเหลือง สีเขียว สีน้ำเงิน และ สีเทา</li>
												</ul>
											</li>
										</ul>
									</span>

									<span class="row">
										<h2>High performance : 4H</h2>
										<ul class="list-bullet  -bold">
											<li>
												High Technology
												<ul>
													<li>ผลิตด้วยเครื่องจักรทันสมัย ผลิตด้วยระบบอุตสาหกรรมแบบครบวงจร ตั้งแต่การตัด เชื่อม ดัดขึ้นรูป ด้วยสุดยอดเทคโนโลยี ควบคุมการผลิตด้วยระบบควบคุมคุณภาพ ISO 9001:2000</li>
												</ul>
											</li>
										</ul>
										<ul class="list-bullet  -bold">
											<li>
												High Rust Protection
												<ul>
													<li>ด้วยกระบวนการชุบ Hot - Dip Galvanized ซึ่งมีประสิทธิภาพในการป้องกันสนิมสูงสุด ทนต่อการกัดกร่อนของน้ำเกลือ หรือการเคลือบผิว ด้วย EPD (Electro ? Deposition Paint) เช่นเดียวกับกระบวนการทำสีป้องกันสนิมอุตสาหกรรมรถยนต์จากนั้นผ่านกระบวนการ Powder Coating ซึ่งมีความหนารวมแล้วไม่ต่ำกว่า 100 ไมครอน ทนต่อรังสี UV สีสันสวยสดแข็งแรงทนทาน และไม่เป็นสนิม</li>
												</ul>
											</li>
										</ul>
										<ul class="list-bullet  -bold">
											<li>
												High Quality
												<ul>
													<li>ท่อเหล็กและลวดรีดเย็นเป็นไปตามมาตรฐานอุตสาหกรรม มอก.747 ? 2531 , มอก.737 ? 2549 , มอก 107-2533</li>
													<li>ASTM A 123, ASTM A 153/A 153M., ASTM B 6, BS 729 หรือ DIN 50976 สำหรับการชุบ Hot ? Dip Galvanized</li>
													<li>ASTM B 117, ASTM G 154 (UVB-313), ASTM D 2794, ISO 2409 สำหรับ Powder Coating</li>
													<li>ทุกขั้นตอนการผลิตควบคุมภายใต้มาตรฐาน ISO : 9001 ? 2000</li>
												</ul>
											</li>
										</ul>
										<ul class="list-bullet  -bold">
											<li>
												High Innovation
												<ul>
													<li>ด้วยนวัตกรรมการผลิต และการออกแบบ โดยทีมงาน วิศวกรที่มีความเชี่ยวชาญ ได้มีการพัฒนาผลิตภัณฑ์อย่างต่อเนื่อง สอดคล้องกับความพึงพอใจของลูกค้า</li>
												</ul>
											</li>
										</ul>
										<ul class="list-bullet  -bold">
											<li>
												รั้วสำเร็จรูป Ezy-Fence มีรูปแบบให้เลือกทั้งหมด 4 แบบ คือ แบบ ST, LT, TR และ DD
												<div style="display: flex;margin-bottom: 10px;">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence01.jpg" alt="...">
												</div>
												<div style="display: flex;">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence02.jpg" alt="...">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence03.jpg" alt="...">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence04.jpg" alt="...">
												</div>
												
												</ul>
											</li>
										</ul>
										<ul class="list-bullet  -bold">
											<li>
												Variety of Choices
												<div style="display: flex;margin-bottom: 10px;">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence05.jpg" alt="...">
												</div>
											</li>
										</ul>
									</span>
									

									<span class="row">
										<h2>Standard and Specification of Hot-dip Galvanized and Powder Coating</h2>
										<ul class="list-bullet  -bold">
											<li>
												Requirement for coating thickness conform to ASTM A 123/A 123M
												<div style="display: flex;margin-bottom: 10px;">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence06.jpg" alt="...">
												</div>
											</li>
											<li>
												Requirements EDP Electro Galvanized Powder Coating for Post,Wire
												<div style="display: flex;margin-bottom: 10px;">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence07.jpg" alt="...">
												</div>
											</li>
										</ul>
									</span>

									<span class="row">
										<h2>ตัวอย่างรั้ว Ezy-fence รุ่น AA และ DD</h2>
										<ul class="list-bullet  -bold">
											<li>
												<center>Type AA</center>
												<div style="display: flex;margin-bottom: 10px;">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence08.jpg" alt="...">
												</div>
											</li>
											<li>
												<center>Type DD</center>
												<div style="display: flex;margin-bottom: 10px;">
													<img style="text-align: center;margin: auto; width: auto;" src="http://www.billionmesh.com/UserFiles/image/ezyfence09.jpg" alt="...">
												</div>
											</li>
											<li>
												รั้วสำเร็จรูป Ezy-Fence เหมาะสำหรับกั้นอาณาเขตล้อมรั้วบ้าน รั้วสนามกีฬา รั้วสนามกอล์ฟ รั้วโรงงานอุตสาหกรรม หน่วยงานราชการ รัฐวิสาหกิจ เพราะมีลักษณะโปร่งสามารถมองเห็นทิวทัศน์ด้านนอกได้ นอกจากนี้ยังมีความสวยงาม ทนทาน ประหยัดทั้งเวลา และค่าใช้จ่าย
											</li>
										</ul>
									</span>

									<span href="#" class="btn-close"></span>
								</div>
							</a>
							<a href="#" class="item">
								<span class="img  hover-img" style="max-height:100px">
									<img src="https://i0.wp.com/bpower.co.th/wp-content/uploads/2018/12/pexels-photo-683402.jpeg" alt="">
								</span>
								<span class="detail">
									<span class="icon">
										<img src="<?= $view->getThemePath() ?>/assets/img/icons/comfort.png" alt="">
									</span>
									<span class="text">
										<p>Wire Mesh</p>
									</span>
								</span>
								<div class="item-content">
									<span class="row">
										<h2>WIRE MESH “ตะแกรงสำเร็จรูป”</h2>
										<ul class="list-bullet  -bold">
											<li>
												ผลิตจากลวดรีดเย็นคุณภาพสูง (Cold Drawn Steel Wire) มีทั้งชนิดผิวกลม และผิวข้ออ้อย ใช้เป็นเหล็กเสริมคอนกรีต ทดแทนการผูกเหล็กเส้นในปัจจุบัน ด้วยคุณสมบัติการรับแรงที่ดีกว่า ในหน้าตัดเดียวกับเหล้กเส้น ธรรมดา จึงส่งผลให้ปริมาณเหล็กน้อยลง ทั้งยังประหยัดแรงงานในการผูก และลดเวลาการทำงาน
												<ul>
													<li>ถูก : ราคาถูกกว่า<br/>WIRE MESH ผลิตจากลวดรีดเย็นคุณภาพสูง (Cold Drawn Steel Wire)ค่ากำลังที่ใช้ในการออกแบบไม่น้อยกว่า 5,500 ksc. ซึ่งแข็งแรงกว่าเหล็กก่อสร้างทั่วไป ที่มีค่ากำลังในการออกแบบ ประมาณ 2,400 ksc. ส่งผลต่อการใช้เหล็กเสริมที่น้อยลง ลดต้นทุนค่าเหล็ก</li>
													<li>เร็ว : ทำงานเร็วกว่า<br/>WIRE MESH ผลิตจากสำเร็จรูปมาจากโรงงาน มีทั้ง แบบม้วน และแบบผืน ออกแบบตามขนาด(Made to Order) ง่ายต่อการทำงานรวดเร็วกว่าผูกเหล็กธรรมดา ลดเวลาการทำงาน</li>
													<li>ดี : มาตรฐานดีกว่า<br/>WIRE MESH กระบวนการผลิตทุกขั้นตอน ตามมาตรฐาน มอก.737-2549 โดยการเชื่อมด้วยไฟฟ้า ใช้เครื่องจักรอัตโนมัติ มีความละเอียดสูง มีการตรวจสอบทุกกระบวนการผลิต มาตรฐานดีกว่าการผูกเหล็กธรรมดา คุณภาพตามมาตรฐาน</li>
												</ul>
											</li>
										</ul>
									</span>

									<span class="row">
										<table border="1"  cellspacing="0" cellpadding="2" style=" text-align: center;
										font-size: 11px;">
											<tbody><tr><td class="txt12" colspan="6">SPECIFICATION</td></tr><tr><td class="txt10">ชนิด</td><td class="txt10">มาตรฐาน</td><td class="txt10">เส้นผ่านศูนย์กลาง (มม.)<br> Diameter. (mm.)</td><td class="txt10">ระยะห่าง (ซม.)<br> Spacing (cm.)</td><td class="txt10">ความกว้าง (ม.)<br> Width (m.)</td><td class="txt10">ความยาว (ม.)<br> Length (m.)</td></tr><tr><td class="txt10">ตะแกรงเหล็กสำเร็จรูป<br> ผิวเรียบ</td><td class="txt10" align="center">มอก.737-2549</td><td class="txt10" align="center">4.0 – 8.0</td><td class="txt10" align="center">5.0 – 40.0</td><td class="txt10" align="center">1.0 – 3.4</td><td class="txt10" align="center">1.0 – 12.0 (ชนิดแผง)<br> 1.0 – 50.0 (ชนิดม้วน)</td></tr><tr><td class="txt10">ตะแกรงเหล็กสำเร็จรูป<br> ผิวข้ออ้อย</td><td class="txt10" align="center">มอก.737-2549</td><td class="txt10" align="center">4.0 – 9.0</td><td class="txt10" align="center">5.0 – 40.0</td><td class="txt10" align="center">1.0 – 3.4</td><td class="txt10" align="center">1.0 – 12.0 (ชนิดแผง)<br> 1.0 – 50.0 (ชนิดม้วน)</td></tr></tbody></table>
									</span>
							
									<span href="#" class="btn-close"></span>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end content this page -->

		<!-- footer => /body to /html [popup inline] -->
		<?php include('inc/footer.php'); ?>
	</div>
	<!--end #wrapper-->

	<!-- javascript => inc all js -->
	<?php include('inc/javascript.php'); ?>

	<!-- start javascript this page -->
	<script>
		/*assets/js/main.js*/
		runScriptWellBuildingPage();
	</script>
	<!-- end javascript this page -->
</body>

</html>