<!DOCTYPE html>
<html>
	<?php include('inc/head.php'); ?>
	<body>
		<!--#wrapper-->
		<div id="wrapper" class="news-page">
			<!-- header => [menu, share top content] -->
			<?php include('inc/header.php'); ?>

			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
				<div class="bg-rectangle-white">
					<div class="section  highlight-content">
						<div class="page-center">
							<div class="title-section">
								<div class="underline">
									<h1>IN THE <span>NEWS</span></h1>
								</div>
								<p>THE LATEST <span>UPDATE</span></p>
							</div>

							<img src="assets/img/news/img-news.jpg" alt="" class="bg">
						</div>
					</div>

					<div class="section  filter  open">
						<div class="page-center">
							<h2>FILTER NEWS :</h2>
							<div class="filter-items  form-style">
								<div class="item  checkbox-style">
									<input type="checkbox" checked id="chk-all">
									<label for="chk-all">All</label>
								</div>
								<div class="item  checkbox-style">
									<input type="checkbox" checked id="chk-press-release">
									<label for="chk-press-release">Press Release</label>
								</div>
								<div class="item  checkbox-style">
									<input type="checkbox" checked id="chk-construction-update">
									<label for="chk-construction-update">Construction update</label>
								</div>
								<div class="item  checkbox-style">
									<input type="checkbox" checked id="chk-event">
									<label for="chk-event">Event</label>
								</div>
							</div>
						</div>
					</div>

					<div class="section  news">
						<div class="page-center">
							<div class="list-content  list-news">
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-01.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-default.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-default.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-01.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-default.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-default.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-01.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-default.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
								<div class="item">
									<a href="news-detail.php" class="img  hover-img">
										<img src="assets/img/uploads/thumb-news-default.jpg" alt="">
									</a>
									<div class="detail">
										<h3>Publisher Name</h3>
										<p class="date">5 / 3 / 2018</p>
										<h2>THE PARQ’S ​GROUND BREAKING</h2>
										<p>Tenants in finance, insurance and property sectors signed 13 relocation or expansion deals over</p>
									</div>
									<div class="bottom">
										<p><small>in</small> <a href="#">Press Release</a></p>
										<div class="group-btn">
											<a href="news-detail.php" class="btn-text">READ MORE</a>
											<a href="#" class="btn-text">SHARE</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="section  pagination">
						<div class="page-center">
							<ul class="pagination">
								<li class="page-item disabled"><span class="page-link">PREVIOUS</span></li>
								<li class="page-item active"><span class="page-link">1</span></li>
								<li class="page-item"><a class="page-link" href="https://www.bbnetworkgroup.com/
								news-and-events?page=2">2</a></li>
								<li class="page-item"><a class="page-link" href="https://www.bbnetworkgroup.com/
								news-and-events?page=3">3</a></li>
								<li class="page-item"><a class="page-link" href="https://www.bbnetworkgroup.com/
								news-and-events?page=2" rel="next">NEXT</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		  <!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php include('inc/footer.php'); ?>
		</div><!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php include('inc/javascript.php'); ?>
		
		<!-- start javascript this page -->
		<script>
			/*assets/js/main.js*/
			runScriptNewsPage();
		</script>
	  <!-- end javascript this page -->
	</body>
</html>