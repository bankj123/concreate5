<?php
defined('C5_EXECUTE') or die("Access Denied.");
define('URL_REWRITING_ALL', true); ?>
<!DOCTYPE html>
<html>

<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('inc/head.php'); ?>

<body>
  <div class="<?= $c->getPageWrapperClass() ?>">
    <!--#wrapper-->
    <div id="wrapper" class="home-page well-building-page">
      <!-- header => [menu, share top content] -->
      <?php
      defined('C5_EXECUTE') or die("Access Denied.");
      $this->inc('inc/header.php'); ?>
      <!-- start content this page -->
      <!--#container-->
      <div role="main" id="container">
        <div id="welcome" class="section  welcome  bg-rectangle-white">
          <div class="page-center">
            <div class="title-section">
              <div class="underline">
                <h1>
                  <?php
                  $a = new Area('Welcome : title');
                  $a->display($c);
                  ?>
                  <!-- WELCOME TO <span>B POWER</span> -->
                </h1>
              </div>
              <p>
                <?php
                $a = new Area('Welcome : semi title');
                $a->display($c);
                ?>
                <!-- SIMPLY THE FUTURE. -->
              </p>
            </div>
            <img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-welcome-02.jpg" alt="" class="desktop-style">
            <img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-welcome-02-mobile.jpg" alt="" class="mobile-style">
          </div>

          <div class="box-description  bg-wood-green">
            <div class="page-center">
              <div class="title-section  light-style">
                <div class="underline">
                  <h1>
                    <?php
                    $a = new Area('Welcome : box1 title');
                    $a->display($c);
                    ?>
                    <!-- บี พาวเวอร์ <span class="clear-bold  clear-color">แฟคตอรี่ แอนด์ คอนสตรัคชั่น</span> -->
                  </h1>
                </div>
              </div>
              <?php
              $a = new Area('Welcome : box1 description');
              $a->display($c);
              ?>
              <!-- หนึ่งในผู้นำด้านการผลิต และจัดจำหน่ายผลิตภัณฑ์ตะแกรงเหล็กสำเร็จรูป ลวดเหล็กรีดเย็น ตะแกรงลวดเหล็กกล้าเสริมคอนกรีต <br>
                ประสบการณ์ในการผลิตตะแกรงเหล็กสำเร็จรูปมานานกว่า 15 ปี ทั้งการผลิตโดยเครื่องจักรที่ทันสมัย <br>
                ผสานเทคโนโลยีพร้อมระบบตรวจสอบ และควบคุมโดยวิศวกรผู้เชี่ยวชาญ -->
            </div>
          </div>
        </div>

        <div id="about-bpower" class="section  selling-points">
          <div class="page-center">
            <div class="title-section">
              <div class="underline">
                <h1>
                  <?php
                  $a = new Area('Welcome : box2 title');
                  $a->display($c);
                  ?></h1>
              </div>
            </div>
          </div>
          <div class="bg-wood-white">
            <div class="slide-selling-points  page-center">
              <div class="owl-carousel">
                <a href="#" class="item">
                  <h2 style="background-color: #065c9d">สินค้าสำเร็จรูป</h2>
                  <span class="img  hover-img"><img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-selling-points-01-white.jpg" alt=""></span>
                  <span class="box-des">
                    <span class="inner">
                      <ul class="list-bullet  -bold">
                        <li>รั้วสำเร็จรูป Ezy Fence</li>
                        <li>ระบบรั้วไฟฟ้า Speedrite</li>
                        <li>กล่องหินลวดตะแกรงเหล็ก GABION</li>
                        <li>Ezy-Liner ผลิตจาก EPDM</li>
                        <li>ราวกันตกและราวกันคนข้าม</li>
                        <li>ตะแกรงเหล็กรางระบายน้ำ</li>
                      </ul>
                      <span class="btn-close  mobile-style"></span>
                    </span>
                  </span>
                </a>
                <a href="#" class="item">
                  <h2 style="background-color: #e24e4e">งานโครงการ</h2>
                  <span class="img  hover-img"><img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-selling-points-02-white.jpg" alt=""></span>
                  <span class="box-des">
                    <span class="inner">
                      <ul class="list-bullet  -bold">
                        <li>ศึกษาวางแผนแม่บท (Master Plan)</li>
                        <li>วางแผนโครงการ
                          <br>
                          <br>
                          <p>ในขั้นนี้เป็นการศึกษาเพื่อวิเคราะห์ภาพรวมในการพัฒนาโครงการ Feasibility Study ทำการสำรวจและเก็บข้อมูลในส่วนที่เห็นว่าจำเป็นและสำคัญต่อความเป็นไปได้ของโครงการ โดยวิธีที่ไม่ยุ่งยากและสิ้นเปลืองค่าใช้จ่ายมากนัก
                          </p><br>
                        </li>
                      </ul>
                      <span class="btn-close  mobile-style"></span>
                    </span>
                  </span>
                </a>
                <a href="#" class="item">
                  <h2 style="background-color: #ffcc20">งานวิศวกรรม</h2>
                  <span class="img  hover-img"><img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-selling-points-03-white.jpg" alt=""></span>
                  <span class="box-des">
                    <span class="inner">
                      <ul class="list-bullet  -bold">
                        <li>บริการให้คำปรึกษาและแนะนำการออกแบบ</li>
                        <li>งานตรวจวินิจฉัยหรืองานตรวจรับรองงาน</li>
                        <li>บริการให้คำปรึกษาและแนะนำการออกแบบ</li>
                        <li>ดำเนินงานโดยวิศวกรที่ปรึกษา
                          <p>ซึ่งมีความชำนาญเฉพาะทาง</p>
                        </li>
                      </ul>
                      <span class="btn-close  mobile-style"></span>
                    </span>
                  </span>
                </a>
              </div>
            </div>
          </div>
          <div class="page-center">
            <div class="content align-center">
              <div class=description>
                <p>ด้วยประสบการณ์ในการผลิตตะแกรงเหล็กสำเร็จรูปมานานกว่า 10 ปี การออกแบบและพัฒนาผลิตภัณฑ์อย่างเป็นระบบ ผสานกับการผลิตโดยเครื่องจักรที่ทันสมัยด้วยเทคโนโลยีพร้อมระบบตรวจสอบและควบคุมด้วยวิศวกรผู้เชี่ยวชาญเฉพาะผลิตภัณฑ์ ทำให้ผลิตภัณฑ์ของเราเป็นผู้นำในตลาดประเภทเดียวกัน จนเป็นที่ยอมรับอย่างกว้างขวางทั้งจากหน่วยงานภาครัฐ บริษัทผู้รับเหมาก่อสร้างชั้นนำของประเทศ และผู้บริโภคทั่วไป</p>
                <br />
                <a href="<?php echo View::url('/about'); ?>" class="btn-style" style="width:200px">อ่านต่อ</a>
              </div>
            </div>
          </div>
        </div>

        <div id="product-service" class="section  bg-rectangle-white">
          <div class="page-center">
            <div class="title-section">
              <div class="underline">
                <h1><span>สินค้า</span>และบริการ</h1>
              </div>
            </div>
            <div class="content  align-center">
              <p><span class="green">บริษัท บี เพาเวอร์ แฟคตอรี่ แอนด์ คอนสตรัคชั่น จำกัด</span> มีเป้าหมายการดำเนินธุรกิจ คือ “เป็นผู้นำด้านการผลิต และจัดจำหน่าย รวมถึงการบริการด้านการติดตั้งอย่างครบวงจรของรั้วตะแกรงเหล็กสำเร็จรูป (ezy-fence) และเหล็กสำเร็จรูปสำหรับงานก่อสร้าง (Cut&Bend, Wire Mesh, Engineering Mesh) และสินค้าอุตสาหกรรมเหล็กก่อสร้างอื่นๆ เพื่อรองรับการเติบโตของการก่อสร้างหลักในประเทศไทย และภูมิภาคอาเซียน ” ภายใต้เป้าหมายดังกล่าวบริษัทได้ดำเนินกลยุทธ์เพื่อการสร้างมูลค่าเพิ่ม (Value Added) ให้กับผลิตภัณฑ์เหล็กสำเร็จรูปเพื่อการก่อสร้าง เพิ่มความสะดวก รวดเร็ว ให้กับลูกค้า</p>
              <br />
              <a href="<?php echo View::url('/product-service'); ?>" class="btn-style" style="width:200px">อ่านต่อ</a>
            </div>
            <div id="product-bpower" class="box-core-features">
              <div class="title-section">
                <h1>สินค้าของ <span>บี เพาเวอร์</span></h1>
              </div>

              <div class="list-core-features">
                <a href="#" class="item">
                  <span class="img  hover-img">
                    <img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-comfort.jpg" alt="">
                  </span>
                  <span class="detail">
                    <span class="icon">
                      <img src="<?= $view->getThemePath() ?>/assets/img/icons/comfort.png" alt="">
                    </span>
                    <span class="text">
                      <p>EZY-LINER</p>
                    </span>
                  </span>
                  <div class="item-content">
                    <span class="row">
                      <h2>Ezy-Liner ผลิตจาก EPDM </h2>
                      <ul class="list-bullet">
                        <li>
                          <ul>
                            <li>
                              Ezy-Liner ผลิตจาก EPDM (Ethylene propylene diene terpolymer) ซึ่งเกิดจากการสังเคราะห์รวมตัวกันทางเคมีระหว่าง Etylene กับ Propylene ภายใต้มาตรฐานการควบคุมคุณภาพ จาก Prescott, AR และ Kingstree, Ezy-Liner เป็นผลิตภัณฑ์ที่นำเข้าจากต่างประเทศ ซึ่งผลิต จากบริษัท Firestone ได้รับรองมาตรฐาน ISO 9001: 2000ด้วยเหตุนี้แผ่นยางสังเคราะห์ EDPM จึงเป็นที่นิยมและยอมรับจากทั่วโลก
                            </li>
                          </ul>
                        </li>

                      </ul>
                    </span>
                    <span class="row">
                      <h2>คุณสมบัติของ EPDM</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          High flexibility
                          <ul>
                            <li>มีความยืดหยุ่นสูงแม้ในสภาพอุณหภูมิที่ต่ำถึง – 45 oC มีอายุการใช้งานยาวนาน ไม่น้อยกว่า 30 ปี แม้ภายใต้อุณหภูมิที่เปลี่ยนแปลงไปตามภูมิประเทศ</li>
                          </ul>
                        </li>
                        <li>
                          High elongation
                          <ul>
                            <li>สามารถยืดขยายตัวได้ถึง 300% เหมาะกับการนำไปใช้กับงาน Sub grede</li>
                          </ul>
                        </li>
                        <li>
                          Puncture resistance
                          <ul>
                            <li>ทนต่อแรงกระแทกของน้ำได้ดีกว่า เมื่อเปรียบเทียบกับวัสดุ Lining อื่นๆ</li>
                          </ul>
                        </li>
                        <li>
                          Superior weathering resistance
                          <ul>
                            <li>สามารถทนต่อรังสี UV และ Ozone มีอายุการใช้งานภายใต้ สภาวะปกติมากกว่า 30 ปี</li>
                          </ul>
                        </li>
                        <li>
                          Quick and easy installation
                          <ul>
                            <li>ติดตั้งได้ง่าย สะดวก รวดเร็ว มีหน้ากว้างขนาดใหญ่ถึง 15 m. และมีความยาวถึง 61 m. ทำให้การประหยัดต้นทุนในการตัดต่อและ การติดตั้ง</li>
                          </ul>
                        </li>
                        <li>
                          Low maintenance
                          <ul>
                            <li>Ezy-Liner แทบจะไม่ต้องเสียค่าบำรุงรักษาเลย</li>
                          </ul>
                        </li>
                        <li>
                          Environment
                          <ul>
                            <li>friendlyได้รับมาตรฐาน ISO 14001 ระบบจัดการด้านสิ่งแวดล้อม ซึ่งในการผลิตและการใช้ผลิตภัณฑ์ไม่มีผลกระทบหรือทำลาย สิ่งแวดล้อม
                              แผ่นยางมีน้ำหนักเพียง 0.5 ปอนด์ต่อตารางฟุตเท่านั้น สามารถใช้งานได้หลากหลายรูปแบบสามารถตัดต่อใช้ในงานเก่าหรืองานใหม่ได้เป็นอย่างดีและมีมีประสิทธิภาพ</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span class="row">
                      <h2>มาตรฐานผลิตภัณฑ์</h2>
                      <ul class="list-bullet">
                        <li>
                          <ul>
                            <li>Ezy-Liner เป็นผลิตภัณฑ์ที่ผลิตจากบริษัท Firestone ซึ่งได้รับมาตรฐานสากลรองรับจากหลายชาติ ทั้งมาตรฐานคุณสมบัติทางกายภาพและการต่อยึดที่แข็งแรงทนทาน รวมถึงใบรับรองมาตรฐานของประเทศฝรั่งเศส ASQUAL ซึ่งเป็นมาตรฐานที่ใช้ในการอ้างอิง</li>
                          </ul>
                        </li>

                      </ul>
                    </span>
                    <span class="row">
                      <h2>EPDM สามารถใช้ในงานต่างๆ อาทิ เช่น</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          <ul>
                            <li>งานชลประทาน</li>
                            <li>งานอ่างเก็บน้ำ</li>
                            <li>งานบ่อน้ำหรือสระน้ำ</li>
                            <li>งานหลังคา</li>
                            <li>งานกันซึม</li>
                            <li>ใช้งานกับงานสถาปัตย์ และการตกแต่งอ่างเก็บน้ำ บ่อน้ำ</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span class="row">
                      <h2>มาตรฐานผลิตภัณฑ์</h2>
                      <ul class="list-bullet">
                        <li>
                          <ul>
                            <li>Ezy-Liner เป็นผลิตภัณฑ์ที่ผลิตจากบริษัท Firestone ซึ่งได้รับมาตรฐานสากลรองรับจากหลายชาติ ทั้งมาตรฐานคุณสมบัติทางกายภาพและการต่อยึดที่แข็งแรงทนทาน รวมถึงใบรับรองมาตรฐานของประเทศฝรั่งเศส ASQUAL ซึ่งเป็นมาตรฐานที่ใช้ในการอ้างอิง</li>
                          </ul>
                        </li>

                      </ul>
                    </span>
                    <span href="#" class="btn-close"></span>
                  </div>
                </a>
                <a href="#" class="item">
                  <span class="img  hover-img">
                    <img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-air.jpg" alt="">
                  </span>
                  <span class="detail">
                    <span class="icon">
                      <img src="<?= $view->getThemePath() ?>/assets/img/icons/air.png" alt="">
                    </span>
                    <span class="text">
                      <p>GABION</p>
                    </span>
                  </span>
                  <div class="item-content">
                    <span class="row">
                      <h2>Performance - based air quality</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Indoor air quality is guaranteed
                          <ul>
                            <li>Air quality tested after construction to ensure safe indoor air</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span class="row">
                      <h2>Advanced air quality</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Non-smoking policy
                          <ul>
                            <li>Smoke-free not only indoor spaces but also outdoor open spaces</li>
                          </ul>
                        </li>
                        <li>
                          Preventing air pollutions at entrances
                          <ul>
                            <li>Building pressurization method to prevent air pollutants entering the building</li>
                            <li>Entryway systems at all entrances to capture dirt and particulates</li>
                          </ul>
                        </li>
                        <li>
                          Advanced air system
                          <ul>
                            <li>Dedicated Outdoor Air System (DOAS) for optimal air quality and thermal comfort</li>
                            <li>Increased ventilation – (30% increased over international standard)</li>
                          </ul>
                        </li>
                        <li>
                          Ability to advance air filtration
                          <ul>
                            <li>Additional AHU space and fan capacity foraccommodating carbon filter</li>
                          </ul>
                        </li>
                        <li>
                          Indoor Pollution isolation
                          <ul>
                            <li>Negative pressure and direct exhaust fan for spaces containing all chemicals to prevent air contaminants</li>
                          </ul>
                        </li>
                        <li>
                          Safe interior materials
                          <ul>
                            <li>Low VOC (Volatile Organic Compounds) and low emission materials for paints, coatings, adhesives, sealants, flooring, composite wood and insulation for healthy indoor air quality</li>
                          </ul>
                        </li>
                        <li>
                          Preventing pesticide and herbicides contaminant
                          <ul>
                            <li>No hazardous pesticide and herbicides at outdoor landscape</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span class="row">
                      <h2>Maintenance in the operation stage</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Regular inspection and maintenance for air systems and indoor spaces
                          <ul>
                            <li>Microbe and mold control at cooling coils and interior spaces</li>
                            <li>Frequent maintenance at air filtration system</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span href="#" class="btn-close"></span>
                  </div>
                </a>
                <a href="#" class="item">
                  <span class="img  hover-img">
                    <img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-nou-rishment.jpg" alt="">
                  </span>
                  <span class="detail">
                    <span class="icon">
                      <img src="<?= $view->getThemePath() ?>/assets/img/icons/nou-rishment.png" alt="">
                    </span>
                    <span class="text">
                      <p>GUARD RAIL</p>
                    </span>
                  </span>
                  <div class="item-content">
                    <span class="row">
                      <h2>Performance - based air quality</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Indoor air quality is guaranteed
                          <ul>
                            <li>Air quality tested after construction to ensure safe indoor air</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span class="row">
                      <h2>Advanced air quality</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Non-smoking policy
                          <ul>
                            <li>Smoke-free not only indoor spaces but also outdoor open spaces</li>
                          </ul>
                        </li>
                        <li>
                          Preventing air pollutions at entrances
                          <ul>
                            <li>Building pressurization method to prevent air pollutants entering the building</li>
                            <li>Entryway systems at all entrances to capture dirt and particulates</li>
                          </ul>
                        </li>
                        <li>
                          Advanced air system
                          <ul>
                            <li>Dedicated Outdoor Air System (DOAS) for optimal air quality and thermal comfort</li>
                            <li>Increased ventilation – (30% increased over international standard)</li>
                          </ul>
                        </li>
                        <li>
                          Ability to advance air filtration
                          <ul>
                            <li>Additional AHU space and fan capacity foraccommodating carbon filter</li>
                          </ul>
                        </li>
                        <li>
                          Indoor Pollution isolation
                          <ul>
                            <li>Negative pressure and direct exhaust fan for spaces containing all chemicals to prevent air contaminants</li>
                          </ul>
                        </li>
                        <li>
                          Safe interior materials
                          <ul>
                            <li>Low VOC (Volatile Organic Compounds) and low emission materials for paints, coatings, adhesives, sealants, flooring, composite wood and insulation for healthy indoor air quality</li>
                          </ul>
                        </li>
                        <li>
                          Preventing pesticide and herbicides contaminant
                          <ul>
                            <li>No hazardous pesticide and herbicides at outdoor landscape</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span class="row">
                      <h2>Maintenance in the operation stage</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Regular inspection and maintenance for air systems and indoor spaces
                          <ul>
                            <li>Microbe and mold control at cooling coils and interior spaces</li>
                            <li>Frequent maintenance at air filtration system</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span href="#" class="btn-close"></span>
                  </div>
                </a>
                <a href="#" class="item">
                  <span class="img  hover-img">
                    <img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-mind.jpg" alt="">
                  </span>
                  <span class="detail">
                    <span class="icon">
                      <img src="<?= $view->getThemePath() ?>/assets/img/icons/mind.png" alt="">
                    </span>
                    <span class="text">
                      <p>CANTILEVER<br />GATE</p>
                    </span>
                  </span>
                  <div class="item-content">
                    <span class="row">
                      <h2>Performance - based air quality</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Indoor air quality is guaranteed
                          <ul>
                            <li>Air quality tested after construction to ensure safe indoor air</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span class="row">
                      <h2>Advanced air quality</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Non-smoking policy
                          <ul>
                            <li>Smoke-free not only indoor spaces but also outdoor open spaces</li>
                          </ul>
                        </li>
                        <li>
                          Preventing air pollutions at entrances
                          <ul>
                            <li>Building pressurization method to prevent air pollutants entering the building</li>
                            <li>Entryway systems at all entrances to capture dirt and particulates</li>
                          </ul>
                        </li>
                        <li>
                          Advanced air system
                          <ul>
                            <li>Dedicated Outdoor Air System (DOAS) for optimal air quality and thermal comfort</li>
                            <li>Increased ventilation – (30% increased over international standard)</li>
                          </ul>
                        </li>
                        <li>
                          Ability to advance air filtration
                          <ul>
                            <li>Additional AHU space and fan capacity foraccommodating carbon filter</li>
                          </ul>
                        </li>
                        <li>
                          Indoor Pollution isolation
                          <ul>
                            <li>Negative pressure and direct exhaust fan for spaces containing all chemicals to prevent air contaminants</li>
                          </ul>
                        </li>
                        <li>
                          Safe interior materials
                          <ul>
                            <li>Low VOC (Volatile Organic Compounds) and low emission materials for paints, coatings, adhesives, sealants, flooring, composite wood and insulation for healthy indoor air quality</li>
                          </ul>
                        </li>
                        <li>
                          Preventing pesticide and herbicides contaminant
                          <ul>
                            <li>No hazardous pesticide and herbicides at outdoor landscape</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span class="row">
                      <h2>Maintenance in the operation stage</h2>
                      <ul class="list-bullet  -bold">
                        <li>
                          Regular inspection and maintenance for air systems and indoor spaces
                          <ul>
                            <li>Microbe and mold control at cooling coils and interior spaces</li>
                            <li>Frequent maintenance at air filtration system</li>
                          </ul>
                        </li>
                      </ul>
                    </span>
                    <span href="#" class="btn-close"></span>
                  </div>
                </a>
              </div>
            </div>
            <div class="box-certification -leed">
              <iframe width="100%" height="100%" src="http://www.youtube.com/embed/XzpIKcyVA_k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>



          <div class="section  project-info">
            <div class="page-center">
              <div class="content">
                <div class="detail">
                  <div class="title-section">
                    <div class="underline">
                      <h1>สินค้าของเรา</h1>
                    </div>
                    <p>บริษัท บี เพาเวอร์ แฟคตอรี่ แอนด์ คอนสตรัคชั่น จำกัด
                      มุ่งมั่นพัฒนาความเชี่ยวชาญในตัวผลิตภัณฑ์อย่างต่อเนื่อง โดยเพิ่มผลิตภัณฑ์และบริการให้ครบวงจร รวมทั้ง ลูกค้าและผู้รับเหมาก่อสร้าง ถือเป็นหุ้นส่วนทางการค้าของบริษัท
                    </p>
                  </div>
                  <a href="<?php echo View::url('/project-reference'); ?>" class="btn-style">อ่านต่อ</a>
                </div>
                <div class="img">
                  <img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-project-info-01.jpg" alt="" class="desktop-style">
                  <img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-project-info-01-mobile.jpg" alt="" class="mobile-style">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="contact-us" class="section  contact  bg-wood-cream">
          <div class="page-center">
            <div class="title-section">
              <div class="underline">
                <p>B Power Company</p>
              </div>
              <h1>ติดต่อเรา</h1>
            </div>

            <div class="form-style  form-contact">
              <?php
              $a = new Area('Contact Form');
              $a->display($c);
              ?>
              <!-- <form action="">
                <ul>
                  <li>
                    <div class="row">
                      <div class="col">
                        <input type="text" id="name" class="inputtext-style">
                        <label for="name">YOUR NAME :</label>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col">
                        <input type="email" id="email" class="inputtext-style">
                        <label for="email">E-MAIL :</label>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col">
                        <input type="tel" id="phone" class="inputtext-style" maxlength="10">
                        <label for="phone">PHONE :</label>
                      </div>
                    </div>
                  </li>
                  <li class="textarea">
                    <div class="row">
                      <div class="col">
                        <label for="message">MESSAGE :</label>
                        <textarea name="message" id="message" class="textarea-style" placeholder=""></textarea>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col">
                        <button type="submit" class="btn-style  -solid">SUBMIT</button>
                      </div>
                    </div>
                  </li>
                </ul>
              </form> -->
            </div>
          </div>
        </div>
      </div>
      <!-- end content this page -->

      <!-- footer => /body to /html [popup inline] -->
      <?php
      defined('C5_EXECUTE') or die("Access Denied.");
      $this->inc('inc/footer.php'); ?>

    </div>
    <!--end #wrapper-->

    <!-- javascript => inc all js -->
    <?php
    defined('C5_EXECUTE') or die("Access Denied.");
    $this->inc('inc/javascript.php'); ?>

    <!-- start javascript this page -->
    <script>
      /*assets/js/main.js*/
      runScriptHomePage();
      runScriptRetailPage();
      runScriptWellBuildingPage();
      // setTimeout(function() {
      //   $.fancybox({
      //     href: '<?= $view->getThemePath() ?>/assets/img/uploads/img-popup.jpg',
      //     type: 'image',
      //     padding: 0
      //   });
      // }, 500);
    </script>
  </div>
  <!-- end javascript this page -->
  <?php Loader::element('footer_required') ?>
</body>

</html>