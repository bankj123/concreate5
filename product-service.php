<!DOCTYPE html>
<html>
<?php include('inc/head.php'); ?>

<body>
	<!--#wrapper-->
	<div id="wrapper" class="well-building-page">
		<!-- header => [menu, share top content] -->
		<?php include('inc/header.php'); ?>

		<!-- start content this page -->
		<!--#container-->
		<div role="main" id="container">

			<div class="section  bg-rectangle-white">
				<div class="page-center">
					<div class="title-section">
						<div class="underline">
							<h1><span>สินค้า</span> และ บริการ</h1>
						</div>
					</div>
					<div class="content  align-center">
						<p><span class="green">บริษัท บี เพาเวอร์ แฟคตอรี่ แอนด์ คอนสตรัคชั่น จำกัด</span>
							มีเป้าหมายการดำเนินธุรกิจ คือ “เป็นผู้นำด้านการผลิต และจัดจำหน่าย รวมถึงการบริการด้านการติดตั้งอย่างครบวงจรของรั้วตะแกรงเหล็กสำเร็จรูป (ezy-fence)
							และเหล็กสำเร็จรูปสำหรับงานก่อสร้าง (Cut&Bend, Wire Mesh, Engineering Mesh) และสินค้าอุตสาหกรรมเหล็กก่อสร้างอื่นๆ
							เพื่อรองรับการเติบโตของการก่อสร้างหลักในประเทศไทย และภูมิภาคอาเซียน” ภายใต้เป้าหมายดังกล่าวบริษัทได้ดำเนินกลยุทธ์เพื่อการสร้างมูลค่าเพิ่ม (Value Added)
							ให้กับผลิตภัณฑ์เหล็กสำเร็จรูปเพื่อการก่อสร้าง เพิ่มความสะดวก รวดเร็ว ให้กับลูกค้า</p>
						<a href="https://youtu.be/XzpIKcyVA_k" target="_blank" class="btn-style" style="margin-top:20px;width:200px">อ่านต่อ</a>
					</div>
				</div>
			</div>

			<div id="a-well-building" class="section  a-well-building">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1>สินค้าของ <span>บี เพาเวอร์</span></h1>
							</div>
						</div>
						<div class="slide-a-well-building">
							<div class="owl-carousel">
								<a href="#" class="item">
									<span class="img  hover-img">
										<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-air.jpg" alt="" />
									</span>
									<span class="detail">
										<span class="icon">
											<img src="<?= $view->getThemePath() ?>/assets/img/icons/air.png" alt="" />
										</span>
										<span class="text">
											<h2>AIR</h2>
										</span>
									</span>
								</a>
								<a href="#" class="item">
									<span class="img  hover-img">
										<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-water.jpg" alt="" />
									</span>
									<span class="detail">
										<span class="icon">
											<img src="<?= $view->getThemePath() ?>/assets/img/icons/water.png" alt="" />
										</span>
										<span class="text">
											<h2>WATER</h2>
										</span>
									</span>
								</a>
								<a href="#" class="item">
									<span class="img  hover-img">
										<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-light.jpg" alt="" />
									</span>
									<span class="detail">
										<span class="icon">
											<img src="<?= $view->getThemePath() ?>/assets/img/icons/light.png" alt="" />
										</span>
										<span class="text">
											<h2>LIGHT</h2>
										</span>
									</span>
								</a>
								<a href="<?php echo View::url('/product-service/list'); ?>" class="item  btn-more">
									<p>อ่านต่อ</p>
								</a>
							</div>
						</div>
						<a href="#" class="btn-style  mobile-style">อ่านต่อ</a>
					</div>
				</div>
			</div>

		</div>
		<!-- end content this page -->

		<!-- footer => /body to /html [popup inline] -->
		<?php include('inc/footer.php'); ?>
	</div>
	<!--end #wrapper-->

	<!-- javascript => inc all js -->
	<?php include('inc/javascript.php'); ?>

	<!-- start javascript this page -->
	<script>
		/*assets/js/main.js*/
		runScriptHomePage();
    runScriptWellBuildingPage();
	</script>
	<!-- end javascript this page -->
</body>

</html>