<!DOCTYPE html>
<html>
	<?php include('inc/head.php'); ?>
	<body>
		<!--#wrapper-->
		<div id="wrapper" class="retail-page">
			<!-- header => [menu, share top content] -->
			<?php include('inc/header.php'); ?>

			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
				<div class="bg-orange">
					<div class="section  highlight-content">
						<div class="page-center">
							<div class="title-section">
								<div class="underline">
									<h1>SHOP <span>WELL</span> &amp; EAT <span>WELL</span></h1>
								</div>
								<p><span>THE PARQ</span> LIFE</p>
							</div>
							<img src="assets/img/retail/img-retail.jpg" alt="" class="bg">
						</div>
					</div>

					<div class="section  description-highlight-content">
						<div class="page-center">
							<div class="title-section">
								<div class="underline">
									<h1>RETAIL <span>VISION</span></h1>
								</div>
							</div>
							<div class="content">
								<p>The PARQ Retail is poised to be the first <span class="green">Wellness-Centri</span> mall with a myriad of healthy options spanning across our F&smp;B, retail and service offerings over 3 floors of conducive setting designed with sky light, spacious respite area, smart features and curated tenant mix built on the motto <span class="green">"Eat Well and Shop Well"</span> - reaching out to office workers, residents and visitors to the nearby convention and healthcare facilities.</p>
								<p class="align-left">It will be the destination for brands who share our wellness value to congregate, <span class="green">bringing healthy living for all at large</span>; The PARQ Retail will be the choice venue for customers seeking the convenience of daily needs and a comprehensive range of services, it will also be the place where one can eat, shop and relax in a stimulating environment and unwind amidst green and open spaces and a multi-functional roof top garden.</p>
								<div class="quote">
									<div class="inner">
										<p>The PARQ Retail is 
											<br>where the <strong class="green">SYNERGY 
												<br>OF WELL-BALANCED LIFE</strong>
											<br>begins outside home.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section  selling-points">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1><span>POSITIONING</span> AND <span>SELLING</span> POINTS</h1>
							</div>
							<p>EVERYDAY <span>WELL-BEING</span> AND <span>WELL-BALANCED</span> DAILY LIFE</p>
						</div>
						<div class="content  align-center">
							<p>A "Top of Mind" recall mall <span class="green">providing convenience for grocery shopping, daily needs and banking facilities</span>; with <span class="green">curated F&amp;B and Retail selections and conducive shopping ambiance</span> reaching out to office workers and residents in the nearby vicinity, visitors to QSNCC and the healthcare facility. A key differentiator is our <span class="green">special focus on Wellness and Health</span> to complement its proximity to Benjakitti Park - <span class="green">to make healthy living an integral part of our lives - at The PARQ Retail.</span></p>
						</div>
					</div>

					<div class="bg-wood-white">
						<div class="slide-selling-points  page-center">
							<div class="owl-carousel">
								<a href="#" class="item">
									<h2>CONDUCIVE AMBIANCE</h2>
									<span class="img  hover-img"><img src="assets/img/uploads/img-selling-points-01.jpg" alt=""></span>
									<span class="box-des">
										<span class="inner">
											<ul class="list-bullet  -bold">
												<li>Grand Steps</li>
												<li>Green &amp; Open Spaces</li>
												<li>Sky Light</li>
											</ul>
											<span class="btn-close  mobile-style"></span>
										</span>
									</span>
								</a>
								<a href="#" class="item">
									<h2>CONVENIENT VENUE</h2>
									<span class="img  hover-img"><img src="assets/img/uploads/img-selling-points-02.jpg" alt=""></span>
									<span class="box-des">
										<span class="inner">
											<ul class="list-bullet  -bold">
												<li>Grocery Shopping
													<br><p>Provide some degree of grocery shopping or supermarket for daily usage</p><br></li>
												<li>Daily Needs</li>
												<li>Banking Facilities</li>
											</ul>
											<span class="btn-close  mobile-style"></span>
										</span>
									</span>
								</a>
								<a href="#" class="item">
									<h2>CURATED F&amp;B 
										<br>AND RETAIL</h2>
									<span class="img  hover-img"><img src="assets/img/uploads/img-selling-points-03.jpg" alt=""></span>
									<span class="box-des">
										<span class="inner">
											<ul class="list-bullet  -bold">
												<li>F&amp;B – Variety</li>
												<li>Retail - Niche Offerings
													<br><br><p>Provide niche brands restaurants/bars. These are standalone bars/restaurants on other areas which still don’t have many branches but would like to set up 2nd or 3rd branch at The Parq e.g. Khua Kling Pak Sod, Wish bar, etc.</p></li>
											</ul>
											<span class="btn-close  mobile-style"></span>
										</span>
									</span>
								</a>
								<a href="#" class="item">
									<h2>CONDUCIVE AMBIANCE</h2>
									<span class="img  hover-img"><img src="assets/img/uploads/img-selling-points-01.jpg" alt=""></span>
									<span class="box-des">
										<span class="inner">
											<ul class="list-bullet  -bold">
												<li>Grand Steps</li>
												<li>Green &amp; Open Spaces</li>
												<li>Sky Light</li>
											</ul>
											<span class="btn-close  mobile-style"></span>
										</span>
									</span>
								</a>
								<a href="#" class="item">
									<h2>CONVENIENT VENUE</h2>
									<span class="img  hover-img"><img src="assets/img/uploads/img-selling-points-02.jpg" alt=""></span>
									<span class="box-des">
										<span class="inner">
											<ul class="list-bullet  -bold">
												<li>Grocery Shopping
													<br><p>Provide some degree of grocery shopping or supermarket for daily usage</p><br></li>
												<li>Daily Needs</li>
												<li>Banking Facilities</li>
											</ul>
											<span class="btn-close  mobile-style"></span>
										</span>
									</span>
								</a>
								<a href="#" class="item">
									<h2>CURATED F&amp;B 
										<br>AND RETAIL</h2>
									<span class="img  hover-img"><img src="assets/img/uploads/img-selling-points-03.jpg" alt=""></span>
									<span class="box-des">
										<span class="inner">
											<ul class="list-bullet  -bold">
												<li>F&amp;B – Variety</li>
												<li>Retail - Niche Offerings
													<br><br><p>Provide niche brands restaurants/bars. These are standalone bars/restaurants on other areas which still don’t have many branches but would like to set up 2nd or 3rd branch at The Parq e.g. Khua Kling Pak Sod, Wish bar, etc.</p></li>
											</ul>
											<span class="btn-close  mobile-style"></span>
										</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>

				<div class="section  gallery  bg-rectangle-green">
					<div class="title-section  light-style">
						<div class="underline">
							<h1>THE PARQ LIFE</h1>
						</div>
					</div>
					<div class="slide-gallery">
						<div class="owl-carousel">
							<div class="item">
								<img src="assets/img/uploads/img-retail-01.jpg" alt="">
							</div>
							<div class="item">
								<img src="assets/img/uploads/img-retail-02.jpg" alt="">
							</div>
							<div class="item">
								<img src="assets/img/uploads/img-retail-01.jpg" alt="">
							</div>
							<div class="item">
								<img src="assets/img/uploads/img-retail-02.jpg" alt="">
							</div>
						</div>
					</div>
				</div>

				<div class="section  floor-plan">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1><span>RETAIL</span> SPECIFICATION</h1>
								<h2>BUILDING SECTION &amp; FLOOR PLAN</h2>
							</div>
							<p>A comprehensive offering of leisure &amp; lifestyle experiences to meet the needs and aspirations of office workers, residents, visitors and tourists; a must visit destination and a gathering place </p>
						</div>
						<div class="content  -plan">
							<div class="img-plan">
								<div class="img">
									<img src="assets/img/uploads/img-retail-plan.jpg" alt="">
								</div>
								<a href="assets/img/uploads/img-retail-plan-large.jpg" class="btn-style  -plus  fancybox">ENLARGE</a>
								<p class="mobile-style">SELECT FLOOR PLAN</p>
								<div class="nav-slide">
									<a href="#" data-slide-index="0"><span>RETAIL</span> <small>Level 3</small></a>
									<a href="#" data-slide-index="1"><span>RETAIL</span> <small>Level 2</small></a>
									<a href="#" data-slide-index="2"><span>RETAIL</span> <small>Level 1</small></a>
									<a href="#" class="disabled"><span>OFFICE HIGH ZONE</span>
										<br><small>Level 11-16</small></a>
									<a href="#" class="disabled"><span>OFFICE LOW ZONE</span>
										<br><small>Level 4, 6-10</small></a>
									<a href="#" class="disabled"><span>F5</span></a>
								</div>
							</div>
							<div class="slide-plan">
								<div class="slides">
									<div class="item">
										<img src="assets/img/uploads/img-retail-level-3.jpg" alt="">
										<a href="assets/img/uploads/img-retail-level-3-large.jpg" class="btn-style  -plus  fancybox">ENLARGE</a>
									</div>
									<div class="item">
										<img src="assets/img/uploads/img-retail-level-2.jpg" alt="">
										<a href="assets/img/uploads/img-retail-level-2-large.jpg" class="btn-style  -plus  fancybox">ENLARGE</a>
									</div>
									<div class="item">
										<img src="assets/img/uploads/img-retail-level-1.jpg" alt="">
										<a href="assets/img/uploads/img-retail-level-1-large.jpg" class="btn-style  -plus  fancybox">ENLARGE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="description  bg-rectangle-white">
						<div class="page-center">
							<div class="content">
								<div class="col">
									<div class="row">
										<p><strong>No. of storeys :</strong></p>
										<p>16 Storeys, 2 Basement</p>
									</div>
									<div class="row">
										<p><strong>Office floor :</strong></p>
										<p>4 - 16 floor</p>
									</div>
									<div class="row">
										<p><strong>Total lettable area :</strong></p>
										<p>Approx. 60,000 Sq.m.</p>
									</div>
									<div class="row">
										<p><strong>Typical floor plate size :</strong></p>
										<p>Approx. 2,300 - 5,000 Sq.m.</p>
									</div>
									<div class="row">
										<p><strong>Ceiling Height :</strong></p>
										<p>3.00 m</p>
									</div>
									<div class="row">
										<p><strong>Core-to-window depth :</strong></p>
										<p>13.50 m</p>
									</div>
								</div>
								<div class="col">
									<div class="row">
										<p><strong>Passenger lift :</strong></p>
										<p>4 Passenger Lifts, Low zone level 4 - 10 (Capacity 1600 kg.)
											<br>4 Passenger Lifts, High zonelevel 11-16 (Capacity 1600 kg.)</p>
									</div>
									<div class="row">
										<p><strong>Services lift :</strong></p>
										<p>1 Passenger Lifts, All level (Capacity xx00 kg.)</p>
									</div>
									<div class="row">
										<p><strong>Car parking lift :</strong></p>
										<p>2 Passenger Lifts, level B2 - 3 (Capacity xx00 kg.)</p>
									</div>
									<div class="row">
										<p><strong>No. of parking :</strong></p>
										<p>Approx. 800 spaces</p>
									</div>
								</div>
							</div>
							<a href="#" class="btn-style  -down" download>DOWNLOAD FACTSHEET</a>
						</div>
					</div>
				</div>

				<div class="section  key-wellness-features">
					<div class="title-section">
						<div class="underline">
							<h1>KEY <span>WELLNESS</span> FEATURES</h1>
						</div>
						<p>EAT / SHOP / LIVE / BUILD <span>WELL</span></p>
					</div>
					<div class="content  bg-wood-cream">
						<div class="page-center">
							<div class="slide-key-wellness-features">
								<div class="owl-carousel">
									<a href="#" class="item  -f-b  active">
										<span class="img  hover-img">
											<img src="assets/img/uploads/img-key-wellness-features-01.jpg" alt="">
										</span>
										<h2>F&amp;B
											<br><small>EAT WELL</small></h2>
									</a>
									<a href="#" class="item  -retail">
										<span class="img  hover-img">
											<img src="assets/img/uploads/img-key-wellness-features-02.jpg" alt="">
										</span>
										<h2>RETAIL
											<br><small>SHOP WELL</small></h2>
									</a>
									<a href="#" class="item  -facilities">
										<span class="img  hover-img">
											<img src="assets/img/uploads/img-key-wellness-features-03.jpg" alt="">
										</span>
										<h2>FACILITIES
											<br><small>LIVE WELL</small></h2>
									</a>
									<a href="#" class="item  -smart-features">
										<span class="img  hover-img">
											<img src="assets/img/uploads/img-key-wellness-features-04.jpg" alt="">
										</span>
										<h2>SMART FEATURES
											<br><small>BUILD WELL</small></h2>
									</a>
								</div>
							</div>
							<div class="box-expand">
								<div class="item">
									<h2>1</h2>
									<ul class="list-bullet">
										<li>All F&amp;B tenants will offer <strong>at least 4 healthy options</strong> in their menu</li>
										<li>Foodcourt with a healthy theme offering food <strong>with less salt, less sugar and less oil</strong> with at least 3 stalls selling healthy food</li>
										<li>Supermarket to retail <strong>organic, gluten-free, vegan</strong> and other healthy range at 10% higher compared to other supermarkets</li>
										<li><strong>“Eat Well &amp; Shop Well”</strong> Directory featuring all healthy F&amp;B options and wellness-related retail offerings</li>
									</ul>
								</div>
								<div class="item">
									<h2>2</h2>
									<ul class="list-bullet">
										<li>All F&amp;B tenants will offer <strong>at least 4 healthy options</strong> in their menu</li>
										<li>Foodcourt with a healthy theme offering food <strong>with less salt, less sugar and less oil</strong> with at least 3 stalls selling healthy food</li>
										<li>Supermarket to retail <strong>organic, gluten-free, vegan</strong> and other healthy range at 10% higher compared to other supermarkets</li>
										<li><strong>“Eat Well &amp; Shop Well”</strong> Directory featuring all healthy F&amp;B options and wellness-related retail offerings</li>
									</ul>
								</div>
								<div class="item">
									<h2>3</h2>
									<ul class="list-bullet">
										<li>All F&amp;B tenants will offer <strong>at least 4 healthy options</strong> in their menu</li>
										<li>Foodcourt with a healthy theme offering food <strong>with less salt, less sugar and less oil</strong> with at least 3 stalls selling healthy food</li>
										<li>Supermarket to retail <strong>organic, gluten-free, vegan</strong> and other healthy range at 10% higher compared to other supermarkets</li>
										<li><strong>“Eat Well &amp; Shop Well”</strong> Directory featuring all healthy F&amp;B options and wellness-related retail offerings</li>
									</ul>
								</div>
								<div class="item">
									<h2>4</h2>
									<ul class="list-bullet">
										<li>All F&amp;B tenants will offer <strong>at least 4 healthy options</strong> in their menu</li>
										<li>Foodcourt with a healthy theme offering food <strong>with less salt, less sugar and less oil</strong> with at least 3 stalls selling healthy food</li>
										<li>Supermarket to retail <strong>organic, gluten-free, vegan</strong> and other healthy range at 10% higher compared to other supermarkets</li>
										<li><strong>“Eat Well &amp; Shop Well”</strong> Directory featuring all healthy F&amp;B options and wellness-related retail offerings</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section  banner-register  bg-rectangle-green">
					<div class="page-center">
						<div class="content">
							<div class="img">
								<img src="assets/img/uploads/img-register-02.jpg" alt="">
							</div>
							<div class="detail">
								<div class="title-section">
									<div class="underline">
										<h1>INTERESTED IN <span>WELL-LIVING</span></h1>
									</div>
									<p><span>REGISTER</span> FOR MORE INFORMATION</p>
								</div>
								<a href="#" class="btn-style  -solid">REGISTER</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		  <!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php include('inc/footer.php'); ?>
		</div><!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php include('inc/javascript.php'); ?>
		
		<!-- start javascript this page -->
		<script>
			/*assets/js/main.js*/
			runScriptRetailPage();
		</script>
	  <!-- end javascript this page -->
	</body>
</html>