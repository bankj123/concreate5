<!DOCTYPE html>
<html>
	<?php include('inc/head.php'); ?>
	<body>
		<!--#wrapper-->
		<div id="wrapper" class="news-page  -detail">
			<!-- header => [menu, share top content] -->
			<?php include('inc/header.php'); ?>

			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
				<div class="section  bg-rectangle-white">
					<div class="page-center">
						<div class="title-news">
							<h1>LARGE DEALS DOMINATE NEW YORK OFFICE MARKET AS FINANCIAL FIRMS STAFF</h1>
							<p><span class="date">Monday, 12 March 2018</span><span class="author">by <a href="#">Publisher Name</a></span></p>
							<p>In <a href="#">Press Release</a></p>
							<a href="#" class="btn-style  -share">Share to Facebook</a>
						</div>
						<div class="content">
							<img src="assets/img/uploads/img-news-detail.jpg" alt="">
							<p>More than five years after Hudson Yards got its groundbreaking, the megaproject has reached another milestone: According to developers Related Companies and Oxford Properties Group, 15 Hudson Yards, the complex’s first residential building, has topped out.</p>
							<p>The building, designed by Diller Scofidio + Renfro in collaboration with Rockwell Group, stands 917 feet tall—not quite hitting supertall status, but large enough to create an imposing presence. It has 285 apartments, four of which are pricey penthouses located at its crown, along with a bevy of over-the-top amenities that include a 75-foot swimming pool, a screening room, and a co-working space that’s essentially a WeWork for the building’s residents. The building also abuts The Shed, the megaproject’s forthcoming cultural space that’s also designed by DS+R and the Rockwell Group.</p>
							<p>Those apartments hit the market in the fall of 2016, and as of this writing, more than half of the units have sold; Related CEO Jeff Blau said in a statement that interest “has far exceeded everyone’s expectations.”</p>
							<p class="align-center"><a href="http://theparq.com/news">Publisher website : http://theparq.com/news</a></p>
						</div>
					</div>
					<a href="news.php" class="btn-style  -back">READ ALL NEWS</a>
				</div>
			</div>
		  <!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php include('inc/footer.php'); ?>
		</div><!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php include('inc/javascript.php'); ?>
		
		<!-- start javascript this page -->
		<script>
			/*assets/js/main.js*/
			runScriptNewsPage();
		</script>
	  <!-- end javascript this page -->
	</body>
</html>