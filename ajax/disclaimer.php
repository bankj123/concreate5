<div class="popup  popup-disclaimer">
	<div class="inner">
		<div class="title-section">
			<div class="underline">
				<h1>WEBSITE <span>DISCLAIMER</span></h1>
			</div>
		</div>
		<div class="content">
			<p>FULL SIZE IMAGE DISCLAIMER (usually written along the bottom of the image)</p>
			<p>Artist’s impression only. Image, information and specifications may be subject to change. May include elements not provided and serve only as indications of general concepts.</p>
			<br>
			<h2>LEGAL** DISCLAIMER</h2>
			<p>COPYRIGHT AND SITE OWNERSHIP
				<br>&copy; 2017Kasemsubsiri Co., Ltd. All Rights Reserved</p>
			<p>The information contained in this website is for general information purposes only, provided by Kasemsubsiri Co., Ltd. (“KSS”) and/or their affiliated or subsidiaries (if any).</p>
			<p>Images, pictures and diagrams are actual or simulated (as the case may be). Images and specifications may be subject to change or amendment.  We reserve the right to make adjustments to any amendment without advance notice   due to the result of changes to regulations or laws at time of construction; and/or, inconsistencies arising from illustrations or artist impressions which shall serve only as indications for general concepts of the project.</p>
			<p>While we endeavor to keep the information up to date and correct, we make no representations or warranties of any kind, expressed or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained. on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>
			<p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.</p>
			<p>Through this website you may be able to link to other websites or content which are not under the control of us. We have no control over the nature and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.</p>
			<p>We take no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.</p>
			<br>
			<h2>PRIVACY POLICY</h2>
			<p>This privacy policy is provided for the purpose of clarification and management the use of personal information.</p>
			<p>We are committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then it will only be used in accordance with this privacy statement.  We may change this policy from time to time, this policy is currently effective from 1 December 2017.</p>
			<p>We may collect your information including contact information, demographic information or other relevant information. We may periodically send emails using the email address which you have provided. We may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail.</p>
			<p>We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>
			<p>A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences. We use traffic log cookies to identify which pages are being used. This helps us analyze data about webpage traffic and improve our website in order to tailor it to customer needs. We use this information for statistical analysis purposes.  You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer, which may prevent you from taking full advantage of the website.</p>
			<p>You may choose to restrict the collection or use of your personal information by indicating you do not want the information used for direct marketing purposes if applicable, or if you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us.  We will not sell, distribute or release your personal information to third parties unless we have your permission or are required by law to do so. </p>
		</div>
	</div>
</div>