<div class="popup  popup-developer-team">
	<div class="inner">
    <div class="img" style="margin-bottom: 20px;">
      <img src="assets/img/uploads/logo-frasers-property.png" alt="">
    </div>
		<div class="title-section">
			<h1>FRASERS PROPERTY</h1>
			<h2>DEVELOPMENT MANAGER</h2>
		</div>
		<div class="content">
			<p>Frasers Property Limited ("Frasers Property"), is a multi-national company that owns, develops and manages a diverse, integrated portfolio of properties. Listed on the Main Board of the Singapore Exchange Securities Trading Limited ("SGX-ST") and headquartered in Singapore, Frasers Property is organised around five asset classes with assets totalling S$28 billion (approx. THB 666 billion) as at 31 December 2017.</p>
			<p>Frasers Property's assets range from residential, retail, commercial and business parks, to industrial and logistics in Singapore, Australia, Europe, China and Southeast Asia. Its well-established hospitality business owns and/or operates serviced apartments and hotels in over 80 cities across Asia, Australia, Europe, the Middle East and Africa. Frasers Property is unified by its commitment to deliver enriching and memorable experiences for customers and stakeholders, leveraging knowledge and capabilities from across markets and property sectors, to deliver value in its multiple asset classes.</p>
			<p><a href="http://www.frasersproperty.com" target="_blank">www.frasersproperty.com</a></p>
		</div>
	</div>
</div>