<!DOCTYPE html>
<html>
	<?php include('inc/head.php'); ?>
	<body>
		<!--#wrapper-->
		<div id="wrapper" class="register-page">
			<!-- header => [menu, share top content] -->
			<?php include('inc/header.php'); ?>

			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
				<div class="section  register  bg-wood-cream">
					<div class="page-center">
						<div class="title-section">
							<p>THE PARQ</p>
							<div class="upperline">
								<h1>REGISTER</h1>
							</div>
						</div>

						<div class="form-style  form-register">
							<form action="">
								<ul>
									<li class="radio-style">
										<p>INTERESTED IN :</p>
										<div class="row">
											<div class="col">
                                            <!--<div class="col  error">-->
												<input type="radio" name="interested" id="radio_office">
												<label for="radio_office">OFFICE</label>
											</div>
											<div class="col">
												<input type="radio" name="interested" id="radio_retail">
												<label for="radio_retail">RETAIL</label>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
                                            <div class="col">
												<input type="text" id="name" class="inputtext-style">
												<label for="name">YOUR NAME :</label>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
                                            <div class="col">
												<input type="email" id="email" class="inputtext-style">
												<label for="email">E-MAIL :</label>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
                                            <div class="col">
												<input type="tel" id="phone" class="inputtext-style" maxlength="10">
												<label for="phone">PHONE :</label>
											</div>
										</div>
									</li>
									<li class="bg-rectangle-white">
										<div class="row">
											<div class="col">
												<button type="submit" class="btn-style  -solid">REGISTER</button>
											</div>
										</div>
									</li>
								</ul>
							</form>
						</div>
					</div>
				</div>
			</div>
		  <!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php include('inc/footer.php'); ?>
		</div><!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php include('inc/javascript.php'); ?>

		<!-- start javascript this page -->
		<script>
			/*assets/js/main.js*/
			runScriptRegisterPage();
		</script>
	  <!-- end javascript this page -->
	</body>
</html>