<!DOCTYPE html>
<html>
	<?php include('inc/head.php'); ?>
	<body>
		<!--#wrapper-->
		<div id="wrapper" class="location-page">
			<!-- header => [menu, share top content] -->
			<?php include('inc/header.php'); ?>

			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
				<div class="section  highlight-content">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1><span>QSNCC</span> NODE</h1>
							</div>
							<p>Site is surrounded with business district, commercial district, distribution logistic hub, communities and <span>Queen Sirikit National Convention Center</span> and directly connected to <span>MRT QSNCC</span> station.</p>
						</div>
						<div class="content">
							<img src="assets/img/location/img-location.jpg" alt="" class="img-location">
							<img src="assets/img/location/description-location.png" alt="" class="description-location">
							<a href="#" class="btn-style  -solid  btn-hide">HIDE DESCRIPTIONS</a>
							<a href="#" class="btn-style  -solid  btn-show">SHOW DESCRIPTIONS</a>
						</div>
					</div>
				</div>

				<div class="section  description-highlight-content  bg-rectangle-white">
					<div class="page-center">
						<p>With a land area of 48,876 sq. m. in prime location at the corner of the intersection of <span class="green">Ratchadapisek and Rama IV</span> across to FYI center. The property is a 7+30+30 years leasehold from CPB. Site is surrounded with business district, commercial district, distribution logistic hub, communities and national convention center</p>
					</div>
				</div>

				<div class="section  location-detail">
					<div class="page-center">
						<div class="content">
							<div class="col">
								<p><strong>Maximizing <span class="green">accessibility</span> to the site :</strong>
									<br>Vehicle can access along Ratchadapisek Road and Rama IV Road
									<br>Direct connection to major expressway that connects every suburb as well as airports to the CBD.</p>
							</div>
							<div class="col">
								<p><strong>Directly connected to <span class="green">MRT QSNCC station</span></strong>
									<br>and close to MRT Klongtoey station that connects to BTS line at Sukhumvit interchange station</p>
							</div>
						</div>
					</div>
				</div>

				<div class="section  location-detail  -card-style  bg-wood-green">
					<div class="page-center">
						<div class="content">
							<div class="col">
								<div class="img">
									<img src="assets/img/uploads/img-location-01.jpg" alt="">
								</div>
								<div class="detail">
									<h2><span class="green">STRATEGIC</span><span>LOCATION</span></h2>
									<ul class="list-bullet">
										<li>Direct link to <span class="green">MRT QSNCC Station</span> with covered walkways connecting the complex</li>
										<li>Nearby expressway access and proximity to historic Port</li>
										<li>Multiple accesses to <span class="green">Rama IV</span> and <span class="green">Ratchadapisek roads</span></li>
										<li><span class="green">The eastern CBD</span>; with easy access to other key business districts such as <span class="green">Silom-Sathorn</span>, <span class="green">Ploenchit</span>, and <span class="green">Sukhumvit</span></li>
									</ul>
								</div>
							</div>
							<div class="col">
								<div class="img">
									<img src="assets/img/uploads/img-location-02.jpg" alt="">
								</div>
								<div class="detail">
									<h2><span>A REVITALIZED</span>
										<br><span class="green">DOWNTOWN COMMUNITY</span></h2>
									<ul class="list-bullet">
										<li>Maximum connectivity for a seamless lifestyle, work and play</li>
										<li>The only large scale MICE facility downtown</li>
										<li>Exciting retail offering for tenants, locals and Bangkokians</li>
										<li>A people centric development plan, focused on health, wellbeing, convenience and community</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section  map-location  bg-rectangle-white">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1><span class="clear-bold">THE PARQ</span> MAP &amp; ADDRESS</h1>
							</div>
							<p>Corner of Ratchadapisek Road and Rama IV 
								<br>2525 Rama 4 Road, Klongtoei, Klongtoei District, Bangkok 10110 | Tel. 66 (0) 2 260 2525</p>
						</div>
						<div class="content">
							<div class="img">
								<img src="assets/img/uploads/map.jpg" alt="">
							</div>
							<div class="group-btn">
								<a href="assets/img/uploads/map.jpg" class="btn-style  -download" download>DOWNLOAD MAP</a>
								<a href="#" class="btn-style  -pin">VIEW GOOGLE MAP</a>
							</div>
						</div>
					</div>
				</div>

				<div class="section  project-info">
					<div class="page-center">
						<div class="content">
							<div class="detail">
								<div class="title-section">
									<div class="underline">
										<h2>THE PARQ</h2>
										<h1>THE PARQ VISION</h1>
									</div>
									<p>At the intersection of innovative architecture, sustainable design and integrated functionality, The PARQ redefines what it means to work, live and play in Bangkok.</p>
								</div>
								<a href="#" class="btn-style">READ MORE</a>
							</div>
							<div class="img">
								<img src="assets/img/uploads/img-project-info-01.jpg" alt="" class="desktop-style">
								<img src="assets/img/uploads/img-project-info-01-mobile.jpg" alt="" class="mobile-style">
							</div>
						</div>
					</div>
				</div>
			</div>
		  <!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php include('inc/footer.php'); ?>
		</div><!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php include('inc/javascript.php'); ?>
		
		<!-- start javascript this page -->
		<script>
			/*assets/js/main.js*/
			runScriptLocationPage();
		</script>
	  <!-- end javascript this page -->
	</body>
</html>