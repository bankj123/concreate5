jQuery(function($){
	// Check Desktop or Mobile
	if($.browser.desktop){
		$('html').addClass('desktop');
	} else if($.browser.mobile){
		$('html').addClass('mobile');
	}
	// End Check Desktop or Mobile


	// Check IE
	if($.browser.msie){
		$('html').addClass('ie');
	}
	// End Check IE


	// Check Browser Version
	/*console.log($.browser.version);*/
	// End Check Browser Version
	

	// Check useragents
	/*jQBrowser.uaMatch();*/
	// End Check useragents


	// Set Open Menu Hamburger
	$('.hamburger').click(function(){
		$('html').toggleClass('open-menu');
		$(this).toggleClass('active');
		return false;
	});
	// End Set Open Menu Hamburger


	// Accordion Content
	$('.accordion .item > a').click(function(){
		if($(this).parents('.item').hasClass('active')){
			$('.item-content').slideUp(400, 'easeInOutQuart').parents('.item').removeClass('active');
		} else{
			$('.item-content').slideUp(400, 'easeInOutQuart').parents('.item').removeClass('active');
			$(this).next('.item-content').slideDown(400, 'easeInOutQuart').parents('.item').addClass('active');
		}
		return false;
	});
	// End Accordion Content


	$(window).scroll(function(event){
		var st = $(this).scrollTop();
		if(st >= 50){
			$('#header').addClass('mini');
		} else{
			$('#header').removeClass('mini');
		}
	});


	$('.btn-to-top').click(function(){
		$('html, body').animate({scrollTop:0}, 800, 'easeInOutQuart');

		return false;
	});

	$('#header .btn-search').click(function(){
		$('#header .box-search').toggleClass('open-search');
		return false;
	});


	$('.menu-page').onePageNav({
		currentClass:'active',
		changeHash:true,
		scrollSpeed:800,
		scrollThreshold:0.5,
		filter:'',
		easing:'easeInOutQuart'
	});


	$('.fancybox').fancybox({
		padding:0
  });


  $('.slide-gallery .owl-carousel').owlCarousel({
		autoplay:false,
		autoplayTimeout:6000,
		smartSpeed:800,
		fallbackEasing:'easeInOutQuart',
    center:true,
    items:1,
    loop:true,
    margin:0,
    nav:true,
    dots:true
	});


	$('.slide-plan .slides').bxSlider({
		mode:'vertical',
		speed:800,
		easing:'easeInOutQuart',
		touchEnabled:false,
		infiniteLoop:false,
		controls:false,
		pagerCustom:'.content.-plan .nav-slide'
	});


	$('.filter h2').click(function(){
		$('.filter').toggleClass('open');
	});


	$('.form-style .inputtext-style').focus(function(){
		$(this).parents('.col').addClass('focus');
	});
	$('.form-style .inputtext-style').blur(function(){
		if($(this).val() == ""){
			$(this).parents('.col').removeClass('focus');
		}
	});
});





/* Function in Page */

// Home Page
function runScriptHomePage(){
	// setTimeout(function(){
	// 	popupIntro();
	// }, 500);


	$('.slide-future-is-well .owl-carousel').owlCarousel({
		autoplay:true,
		autoplayTimeout:6000,
		smartSpeed:800,
		fallbackEasing:'easeInOutQuart',
    loop:true,
    center:false,
    items:1,
    margin:0,
    nav:true,
    dots:false,
    autoWidth:false
	});

	$('.slide-a-well-building .owl-carousel').owlCarousel({
		autoplay:false,
		autoplayTimeout:6000,
		smartSpeed:800,
		fallbackEasing:'easeInOutQuart',
    loop:false,
    margin:30,
    nav:false,
    dots:false,
    autoWidth:true,
    responsive:{
      0:{
        items:3,
        nav:true,
        center:true,
        loop:true
      },
      1025:{
      	items:4
      }
    }
	});

	$('.nav-page').onePageNav({
		currentClass:'active',
		changeHash:true,
		scrollSpeed:800,
		scrollThreshold:0.5,
		filter:'',
		easing:'easeInOutQuart'
	});
}
// End Home Page


// About Page
function runScriptAboutPage(){
	$('.list-design-concept .btn-expand').click(function(){
		$('.list-design-concept').addClass('show-expand');
		$(this).parents('.item').addClass('expand');
		return false;
	});
	$('.list-design-concept .btn-close').click(function(){
		$(this).parents('.item').removeClass('expand');
		$('.list-design-concept').removeClass('show-expand');
		return false;
	});

	$('.slide-developer-team .owl-carousel').owlCarousel({
		autoplay:false,
		autoplayTimeout:6000,
		smartSpeed:800,
		fallbackEasing:'easeInOutQuart',
    center:false,
    loop:false,
    margin:100,
    nav:true,
    dots:false,
    responsive:{
    	320:{
    		items:1
    	},
    	480:{
    		items:2,
    		margin:50
    	},
      1024:{
        items:3,
        margin:50
      },
      1025:{
      	margin:100
      }
    }
	});
}
// End About Page


// Well Building Page
function runScriptWellBuildingPage(){
	$('.well-building-page .list-core-features .item').click(function(){
		if($(this).hasClass('active')){
			// $('.well-building-page .list-core-features .item').removeClass('active');
			// $('.well-building-page .list-core-features .item .item-content').slideUp(400, 'easeInOutQuart');
		} else{
			$('.well-building-page .list-core-features .item').removeClass('active');
			$(this).addClass('active');

			$('.well-building-page .list-core-features .item .item-content').slideUp(400, 'easeInOutQuart');
			$(this).find('.item-content').slideDown(400, 'easeInOutQuart');
		}
		return false;
	});

	$('.well-building-page .box-core-features .btn-close').click(function(){
		$('.well-building-page .list-core-features .item').removeClass('active');
		$('.well-building-page .list-core-features .item .item-content').slideUp(400, 'easeInOutQuart');
		return false;
	});

	$('.well-building-page .box-core-features-gallery .btn-close').click(function(){
		$('.well-building-page .list-core-features .item').removeClass('active');
		$('.well-building-page .list-core-features .item .item-content').slideUp(400, 'easeInOutQuart');
		return false;
	});
}
// End Well Building Page


// Office Page
function runScriptOfficePage(){
	$('.box-newest a.btn-style').click(function(){
		$('.box-newest .box-expand').addClass('expand');
		return false;
	});
	$('.box-newest .box-expand .btn-close').click(function(){
		$('.box-newest .box-expand').removeClass('expand');
		return false;
	});
}
// End Office Page


// Retail Page
function runScriptRetailPage(){
	$('.slide-selling-points .owl-carousel').owlCarousel({
		autoplay:false,
		autoplayTimeout:6000,
		smartSpeed:800,
		fallbackEasing:'easeInOutQuart',
    center:true,
    loop:true,
   	autoWidth:true,
    margin:30,
    nav:true,
    dots:false,
    responsive:{
    	320:{
    		items:1,
    		autoWidth:false,
    		center:false
    	},
    	568:{
    		items:1,
    		autoWidth:false,
    		center:false
    	},
      768:{
        items:3
      }
    }
	});
	$('.slide-selling-points .item').click(function(){
		if($(this).hasClass('show-des')){
			$('.slide-selling-points .item').removeClass('show-des');
		} else{
			$('.slide-selling-points .item').removeClass('show-des');
			$(this).addClass('show-des');
		}
		return false;
	});
	$('.slide-selling-points .item .btn-close').click(function(){
		$('.slide-selling-points .item').removeClass('show-des');
		return false;
	});

	$('.slide-key-wellness-features .owl-carousel').owlCarousel({
		autoplay:false,
		autoplayTimeout:6000,
		smartSpeed:800,
		fallbackEasing:'easeInOutQuart',
    margin:30,
    nav:true,
    dots:false,
    responsive:{
    	320:{
    		items:1
    	},
    	480:{
    		items:2
    	},
    	736:{
    		items:3
    	},
      1025:{
        items:4
      }
    }
	});

	$('.slide-key-wellness-features .item').click(function(){
		$('.slide-key-wellness-features .item').removeClass('active');
		$(this).addClass('active');
		$('.slide-key-wellness-features .owl-stage').attr('id','');
		$('.key-wellness-features .box-expand .item').slideUp(400, 'easeInOutQuart');
		if($(this).parents('.owl-item').index() == 1){
			$('.slide-key-wellness-features .owl-stage').attr('id','l2');
			$('.key-wellness-features .box-expand .item').eq(1).slideDown(400, 'easeInOutQuart');
		} else if($(this).parents('.owl-item').index() == 2){
			$('.slide-key-wellness-features .owl-stage').attr('id','l3');
			$('.key-wellness-features .box-expand .item').eq(2).slideDown(400, 'easeInOutQuart');
		} else if($(this).parents('.owl-item').index() == 3){
			$('.slide-key-wellness-features .owl-stage').attr('id','l4');
			$('.key-wellness-features .box-expand .item').eq(3).slideDown(400, 'easeInOutQuart');
		} else{
			$('.slide-key-wellness-features .owl-stage').attr('id','l1');
			$('.key-wellness-features .box-expand .item').eq(0).slideDown(400, 'easeInOutQuart');
		}
		return false;
	});
}
// End Retail Page


// Location Page
function runScriptLocationPage(){
	$('.highlight-content .content .btn-style').click(function(){
		$('.highlight-content .content').toggleClass('hide-des');
		return false;
	});
}
// End Location Page


// News Page
function runScriptNewsPage(){}
// End News Page


// ContactUs Page
function runScriptContactUsPage(){}
// End ContactUs Page


// ContactUs Page
function runScriptRegisterPage(){}
// End ContactUs Page

/* End Function in Page */





/* All Function */

// Popup Alert
function popupAlert(){
	$.fancybox({
		href:'ajax/popup-alert.php',
		type:'ajax',
		padding:0
	});
}
// End Popup Alert


function popupIntro(){
	$.fancybox({
		href:'assets/img/uploads/img-popup.jpg',
		type:'image',
		padding:0
	});
}

/* End All Function */