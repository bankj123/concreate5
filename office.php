<!DOCTYPE html>
<html>
	<?php include('inc/head.php'); ?>
	<body>
		<!--#wrapper-->
		<div id="wrapper" class="office-page">
			<!-- header => [menu, share top content] -->
			<?php include('inc/header.php'); ?>

			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
				<div class="section  highlight-content  bg-rectangle-white">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1>WORK <span>WELL</span> &amp; LIVE <span>WELL</span></h1>
							</div>
							<p>AT <span>THE PARQ</span> OFFICE</p>
						</div>
						<img src="assets/img/office/img-office.jpg" alt="" class="bg">
					</div>
				</div>

				<div class="section  description-highlight-content  office-vision">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1>OFFICE <span>VISION</span></h1>
							</div>
						</div>
						<div class="content  align-center">
							<p>Unparalleled accessibility, connectivity and convenience underpin the development of the offices at <span class="green">The PARQ</span>, which extends the core of the city’s central business district. Incorporating the best of urban and architectural design, and offering the highest and most sustainable world class standards in facilities, <span class="green">The PARQ offices</span> will serve both international and local top businesses, governments, entrepreneurs, consultants and institutions. As the city’s newest  office complex, <span class="green">The PARQ offices</span> will offer comprehensive features which <span class="green">protect and enhance well-being</span>.</p>
						</div>

						<div class="box-newest">
							<div class="img">
								<img src="assets/img/office/img-office-02.jpg" alt="">
							</div>
							<div class="detail  bg-rectangle-white">
								<div class="title-section">
									<div class="underline">
										<p>THE NEWEST</p>
									</div>
									<h1><span>GRADE A</span>
										<br>OFFICE COMPLEX</h1>
								</div>
								<a href="#" class="btn-style  -solid">
									<p>VIEW</p>
									<h2>SPECIFICATION</h2>
								</a>

								<div class="box-expand">
									<ul class="list-bullet  light-style">
										<li>Grade A global standard architectural design &amp; concept
											<ul>
												<li>Column-free floor plates up to 5,000 sq. m.</li>
												<li>3.0-metre high ceiling</li>
												<li>1.8-metre wide corridors</li>
												<li>Individualised lobbies that are wide with high capacity drop-off area</li>
												<li>Bathroom provision for high density occupancy with hands-free control (auto door) for hygiene</li>
												<li>Shower rooms and disabled toilets are provided on every floor</li>
											</ul>
										</li>                               
										<li>LEED &amp; WELL Certification compliance
											<ul>
												<li>Insulated Low-E Glass Façade</li>
												<li>LED lighting integrated with daylight sensors</li>
												<li>Acoustic ceiling system</li>
												<li>Indoor air-quality controlling system: UV Emitter, MERV 13</li>
											</ul>
										</li>
										<li>Flexible air-conditioning hours</li>
										<li>High capacity destination controlled lifts and security control systems</li>
										<li>World-class property management services including disruption-free maintenance</li>
									</ul>
									<a href="#" class="btn-close  light-style"></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section  gallery  bg-rectangle-green">
					<div class="title-section  light-style">
						<div class="underline">
							<h1>THE OFFICE AT THE PARQ</h1>
						</div>
					</div>
					<div class="slide-gallery">
						<div class="owl-carousel">
							<div class="item">
								<img src="assets/img/uploads/img-office-01.jpg" alt="">
							</div>
							<div class="item">
								<img src="assets/img/uploads/img-office-02.jpg" alt="">
							</div>
							<div class="item">
								<img src="assets/img/uploads/img-office-01.jpg" alt="">
							</div>
							<div class="item">
								<img src="assets/img/uploads/img-office-02.jpg" alt="">
							</div>
						</div>
					</div>
				</div>

				<div class="section  floor-plan">
					<div class="page-center">
						<div class="title-section">
							<div class="underline">
								<h1><span>OFFICE</span> SPECIFICATION</h1>
								<h2>BUILDING SECTION &amp; FLOOR PLAN</h2>
							</div>
							<p>A natural extension of Bangkok's central business district and an emerging preferred office location, the first phase of <span>The PARQ will comprise 66,000 sqm</span> of lettable office space.</p>
						</div>
						<div class="content  -plan">
							<div class="img-plan">
								<div class="img">
									<img src="assets/img/uploads/img-office-plan.jpg" alt="">
								</div>
								<a href="assets/img/uploads/img-office-plan-large.jpg" class="btn-style  -plus  fancybox">ENLARGE</a>
								<p class="mobile-style">SELECT FLOOR PLAN</p>
								<div class="nav-slide">
									<a href="#" data-slide-index="0"><span>OFFICE HIGH ZONE</span>
										<br><small>Level 11-16</small></a>
									<a href="#" data-slide-index="1"><span>OFFICE LOW ZONE</span>
										<br><small>Level 4, 6-10</small></a>
									<a href="#" data-slide-index="2"><span>F5</span></a>
									<a href="#" class="disabled"><span>RETAIL</span>
										<br><small>Level 1-3</small></a>
								</div>
							</div>
							<div class="slide-plan">
								<div class="slides">
									<div class="item">
										<img src="assets/img/uploads/img-office-high-zone.jpg" alt="">
										<a href="assets/img/uploads/img-office-high-zone-large.jpg" class="btn-style  -plus  fancybox">ENLARGE</a>
									</div>
									<div class="item">
										<img src="assets/img/uploads/img-office-low-zone.jpg" alt="">
										<a href="assets/img/uploads/img-office-low-zone-large.jpg" class="btn-style  -plus  fancybox">ENLARGE</a>
									</div>
									<div class="item">
										<img src="assets/img/uploads/img-office-f5.jpg" alt="">
										<a href="assets/img/uploads/img-office-f5-large.jpg" class="btn-style  -plus  fancybox">ENLARGE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="description  bg-rectangle-white">
						<div class="page-center">
							<div class="content">
								<div class="col">
									<div class="row">
										<p><strong>No. of storeys :</strong></p>
										<p>16 Storeys, 2 Basement</p>
									</div>
									<div class="row">
										<p><strong>Office floor :</strong></p>
										<p>4 - 16 floor</p>
									</div>
									<div class="row">
										<p><strong>Total lettable area :</strong></p>
										<p>Approx. 60,000 Sq.m.</p>
									</div>
									<div class="row">
										<p><strong>Typical floor plate size :</strong></p>
										<p>Approx. 2,300 - 5,000 Sq.m.</p>
									</div>
									<div class="row">
										<p><strong>Ceiling Height :</strong></p>
										<p>3.00 m</p>
									</div>
									<div class="row">
										<p><strong>Core-to-window depth :</strong></p>
										<p>13.50 m</p>
									</div>
								</div>
								<div class="col">
									<div class="row">
										<p><strong>Passenger lift :</strong></p>
										<p>4 Passenger Lifts, Low zone level 4 - 10 (Capacity 1600 kg.)
											<br>4 Passenger Lifts, High zonelevel 11-16 (Capacity 1600 kg.)</p>
									</div>
									<div class="row">
										<p><strong>Services lift :</strong></p>
										<p>1 Passenger Lifts, All level (Capacity xx00 kg.)</p>
									</div>
									<div class="row">
										<p><strong>Car parking lift :</strong></p>
										<p>2 Passenger Lifts, level B2 - 3 (Capacity xx00 kg.)</p>
									</div>
									<div class="row">
										<p><strong>No. of parking :</strong></p>
										<p>Approx. 800 spaces</p>
									</div>
								</div>
							</div>
							<a href="#" class="btn-style  -down" download>DOWNLOAD FACTSHEET</a>
						</div>
					</div>
				</div>

				<div class="section  well-features  bg-sky">
					<div class="title-section">
						<div class="underline">
							<h1><span>WELL</span> FEATURES</h1>
						</div>
						<p><span>Well building standards</span> consider 7 areas of importance.
							<br><span>The PARQ</span> offices will incorporate features and management around these seven characteristics</p>
					</div>
					<div class="page-center">
						<div class="content">
							<div class="list-core-features">
								<div class="item  -air">
									<span class="icon">
										<img src="assets/img/icons/air-light.png" alt="">
										<p>AIR</p>
									</span>
									<span class="des">
										<p>Optimised internal environment for thermal comfort and advanced air quality, achieved by preventing air pollution from entering the building at entrances and advanced internal filtration systems to filter pollutants within</p>
									</span>
								</div>
								<div class="item  -water">
									<span class="icon">
										<img src="assets/img/icons/water-light.png" alt="">
										<p>WATER</p>
									</span>
									<span class="des">
										<p>Water quality in the project is guaranteed by testing after construction is completed and before the building is occupied</p>
									</span>
								</div>
								<div class="item  -light">
									<span class="icon">
										<img src="assets/img/icons/light-light.png" alt="">
										<p>LIGHT</p>
									</span>
									<span class="des">
										<p>Provision for abundaant natural light and low-glare luminance in working spaces, for visual comfort and higher productivity</p>
									</span>
								</div>
								<div class="item  -nourishment">
									<span class="icon">
										<img src="assets/img/icons/nou-rishment-light.png" alt="">
										<p>NOURISHMENT</p>
									</span>
									<span class="des">
										<p>Emphasis on clean food retail offerings with an integrated urban farm on site for education and inspiration</p>
									</span>
								</div>
								<div class="item  -fitness">
									<span class="icon">
										<img src="assets/img/icons/fitness-light.png" alt="">
										<p>FITNESS</p>
									</span>
									<span class="des">
										<p>Provision for outdoor amenities to promote active living</p>
									</span>
								</div>
								<div class="item  -comfort">
									<span class="icon">
										<img src="assets/img/icons/comfort-light.png" alt="">
										<p>COMFORT</p>
									</span>
									<span class="des">
										<p>Minimise exterior noise intrusion dBA with the usage of insulating glass</p>
									</span>
								</div>
								<div class="item  -mind">
									<span class="icon">
										<img src="assets/img/icons/mind-light.png" alt="">
										<p>MIND</p>
									</span>
									<span class="des">
										<p>Environmental elements are integrated within the landscape and interior design, lighting and spatial layout</p>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="section  banner-register  bg-rectangle-green">
					<div class="page-center">
						<div class="content">
							<div class="img">
								<img src="assets/img/uploads/img-register.jpg" alt="">
							</div>
							<div class="detail">
								<div class="title-section">
									<div class="underline">
										<h1>INTERESTED IN <span>WELL-LIVING</span></h1>
									</div>
									<p><span>REGISTER</span> FOR MORE INFORMATION</p>
								</div>
								<a href="#" class="btn-style  -solid">REGISTER</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		  <!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php include('inc/footer.php'); ?>
		</div><!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php include('inc/javascript.php'); ?>
		
		<!-- start javascript this page -->
		<script>
			/*assets/js/main.js*/
			runScriptOfficePage();
		</script>
	  <!-- end javascript this page -->
	</body>
</html>