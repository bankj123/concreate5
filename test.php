<?php defined('C5_EXECUTE') or die("Access Denied.");?>
<!DOCTYPE html>
<html>

<head>
  <?php Loader::element('header_required') ?>
</head>

<body>
  <div class="<?= $c->getPageWrapperClass() ?>">
    <?php
    $a = new Area('Area Name 123');
    $a->display($c);
    ?>
  </div>

  <?php Loader::element('footer_required') ?>
</body>

</html>