<?php
defined('C5_EXECUTE') or die("Access Denied.");
define('URL_REWRITING_ALL', true); ?>
<!DOCTYPE html>
<html>

<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('inc/head.php'); ?>

<body>
	<div class="<?= $c->getPageWrapperClass() ?>">
		<!--#wrapper-->
		<div id="wrapper" class="well-building-page about-page">
			<!-- header => [menu, share top content] -->
			<?php
			defined('C5_EXECUTE') or die("Access Denied.");
			$this->inc('inc/header.php'); ?>
			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
			<div id="product-service" class="bg-rectangle-white">
				<div class="page-center">
					<div id="product-bpower" class="box-core-features-gallery">
						<div class="title-section">
							<div class="underline">
							<h1>ผลงาน</span></h1>
							</div>
						</div>
						<div class="list-core-features">
							<a href="#" class="item">
								<span class="img  hover-img" >
									<img src="http://www.billionmesh.com/UserFiles/image/ezyfence08.jpg" alt="">
								</span>
								<span class="detail">
									<span class="icon">
										<img src="<?= $view->getThemePath() ?>/assets/img/icons/comfort.png" alt="">
									</span>
									<span class="text">
										<p>Gallery 1</p>
									</span>
								</span>
								<div class="item-content">
										<div id="gallery" class="list gallery">
											<div class="slide-gallery">
												<div class="owl-carousel">
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
												</div>
											</div>
										</div>
									<span href="#" class="btn-close"></span>
								</div>
							</a>
							<a href="#" class="item">
								<span class="img  hover-img" >
									<img src="http://www.billionmesh.com/UserFiles/image/ezyfence08.jpg" alt="">
								</span>
								<span class="detail">
									<span class="icon">
										<img src="<?= $view->getThemePath() ?>/assets/img/icons/comfort.png" alt="">
									</span>
									<span class="text">
										<p>Gallery 2</p>
									</span>
								</span>
								<div class="item-content">
										<div id="gallery" class="list gallery">
											<div class="slide-gallery">
												<div class="owl-carousel">
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
												</div>
											</div>
										</div>
									<span href="#" class="btn-close"></span>
								</div>
							</a>
							<a href="#" class="item">
								<span class="img  hover-img" >
									<img src="http://www.billionmesh.com/UserFiles/image/ezyfence08.jpg" alt="">
								</span>
								<span class="detail">
									<span class="icon">
										<img src="<?= $view->getThemePath() ?>/assets/img/icons/comfort.png" alt="">
									</span>
									<span class="text">
										<p>Gallery 3</p>
									</span>
								</span>
								<div class="item-content">
										<div id="gallery" class="list gallery">
											<div class="slide-gallery">
												<div class="owl-carousel">
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
												</div>
											</div>
										</div>
									<span href="#" class="btn-close"></span>
								</div>
							</a>
							<a href="#" class="item">
								<span class="img  hover-img" >
									<img src="http://www.billionmesh.com/UserFiles/image/ezyfence08.jpg" alt="">
								</span>
								<span class="detail">
									<span class="icon">
										<img src="<?= $view->getThemePath() ?>/assets/img/icons/comfort.png" alt="">
									</span>
									<span class="text">
										<p>Gallery 4</p>
									</span>
								</span>
								<div class="item-content">
										<div id="gallery" class="list gallery">
											<div class="slide-gallery">
												<div class="owl-carousel">
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
												</div>
											</div>
										</div>
									<span href="#" class="btn-close"></span>
								</div>
							</a>
							<a href="#" class="item">
								<span class="img  hover-img" >
									<img src="http://www.billionmesh.com/UserFiles/image/ezyfence08.jpg" alt="">
								</span>
								<span class="detail">
									<span class="icon">
										<img src="<?= $view->getThemePath() ?>/assets/img/icons/comfort.png" alt="">
									</span>
									<span class="text">
										<p>Gallery 5</p>
									</span>
								</span>
								<div class="item-content">
										<div id="gallery" class="list gallery">
											<div class="slide-gallery">
												<div class="owl-carousel">
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-01.jpg" alt="">
													</div>
													<div class="item-gallert">
														<img src="<?= $view->getThemePath() ?>/assets/img/uploads/img-gallery-02.jpg" alt="">
													</div>
												</div>
											</div>
										</div>
									<span href="#" class="btn-close"></span>
								</div>
							</a>
						</div>

					</div>
				</div>
			</div>
			</div>
			<!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php
			defined('C5_EXECUTE') or die("Access Denied.");
			$this->inc('inc/footer.php'); ?>

		</div>
		<!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php
		defined('C5_EXECUTE') or die("Access Denied.");

		$this->inc('inc/javascript.php'); ?>

		<!-- start javascript this page -->
		<script>
			runScriptWellBuildingPage();
		</script>
	</div>
	<!-- end javascript this page -->
	<?php Loader::element('footer_required') ?>
</body>

</html>