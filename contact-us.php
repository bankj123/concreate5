<!DOCTYPE html>
<html>
	<?php include('inc/head.php'); ?>
	<body>
		<!--#wrapper-->
		<div id="wrapper" class="contact-us-page">
			<!-- header => [menu, share top content] -->
			<?php include('inc/header.php'); ?>

			<!-- start content this page -->
			<!--#container-->
			<div role="main" id="container">
				<div class="section  contact  bg-rectangle-white">
					<div class="page-center  bg-leaf-cream">
						<div class="title-section">
							<p>THE PARQ</p>
							<div class="upperline">
								<h1>CONTACT</h1>
							</div>
						</div>

						<div class="form-style  form-contact">
							<form action="">
								<ul>
									<li>
										<div class="row">
											<div class="col">
												<input type="text" id="name" class="inputtext-style">
												<label for="name">YOUR NAME :</label>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col">
												<input type="email" id="email" class="inputtext-style">
												<label for="email">E-MAIL :</label>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col">
												<input type="tel" id="phone" class="inputtext-style" maxlength="10">
												<label for="phone">PHONE :</label>
											</div>
										</div>
									</li>
									<li class="radio-style">
										<p>INTERESTED IN :</p>
										<div class="row">
											<div class="col">
												<input type="radio" name="interested" id="radio_general-information">
												<label for="radio_general-information">GENERAL INFORMATION</label>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<input type="radio" name="interested" id="radio_office">
												<label for="radio_office">OFFICE</label>
											</div>
											<div class="col">
												<input type="radio" name="interested" id="radio_retail">
												<label for="radio_retail">RETAIL</label>
											</div>
										</div>
									</li>
									<li class="textarea">
										<div class="row">
											<div class="col">
												<label for="message">MESSAGE :</label>
												<textarea name="message" id="message" class="textarea-style" placeholder="Text sample"></textarea>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col">
												<button type="submit" class="btn-style  -solid">SUBMIT</button>
											</div>
										</div>
									</li>
								</ul>
							</form>
						</div>
					</div>

					<div class="address">
						<div class="page-center">
							<div class="underline">
								<h3>STREET ADDRESS</h3>
								<p>Corner of Ratchadapisek Road and Rama IV
									<br>2525 Rama 4 Road, Klongtoei, Klongtoei District, Bangkok 10110</p>
							</div>
							<h3>TELEPHONE</h3>
							<p>66 (0) 2 260 2525</p>
						</div>
					</div>
				</div>
			</div>
		  <!-- end content this page -->

			<!-- footer => /body to /html [popup inline] -->
			<?php include('inc/footer.php'); ?>
		</div><!--end #wrapper-->

		<!-- javascript => inc all js -->
		<?php include('inc/javascript.php'); ?>
		
		<!-- start javascript this page -->
		<script>
			/*assets/js/main.js*/
			runScriptContactUsPage();
		</script>
	  <!-- end javascript this page -->
	</body>
</html>